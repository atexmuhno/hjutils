// fdt/string.cpp -- cString - a reference counted string class
/* Copyright 1996, 1997 Cybergraphic Systems.  All rights reserved. */

//#ifdef USE_LIBRARY_HPP
//#include "library.hpp"
//#else // USE_LIBRARY_HPP
#include "stdafx.h"
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
//#include "debug/debug.hpp"
//#ifdef FORTIFY
//#include "fortify.h"
//#endif // FORTIFY
//#endif // USE_LIBRARY_HPP

#include "string.hpp"


//
// Construct an empty string.
//

cString::cString()
{
	// Create the new string
	_string = new _srep();
	assert( _string );
}


//
// Construct a cString from a normal C-String. The cString takes a copy.
//

cString::cString( const char *string )
{
	assert( string );

	// Create the new string
	if ( string == NULL )
	{
		_string = new _srep();
	}
	else
	{
		_string = new _srep( string );
	}
	assert( _string );
}


//
// Copy constructor. This is different to the const char * constructor
// as it needs to update the reference counts.
//

cString::cString( const cString &string )
{
	assert( string._string );

	if ( string._string == NULL )
	{
		_string = new _srep();
		assert( _string );
	}
	else
	{
		STRING_LOCK( string._string )

		// Point at the string.
		_string = string._string;

		// Update the number of "references" to this string.
		_string->number++;

		STRING_UNLOCK( string._string )
	}
}


//
// Construct an empty string with a bufSize initial bytes.
//

cString::cString( size_t bufSize )
{
	// Create the new string
	if( bufSize <= 0 )
		bufSize = 1;  // We always allocate the NULL terminator

	_string = new _srep( bufSize );
	assert( _string );

	// Ensure its null terminated
	_string->string[0] = '\0';
}


//
// Construct a cString from a zApp zString. Only available if compiled
// with zApp.
// The cString class *must* compile without zApp.
//

#ifdef ZAPP
cString::cString( const zString &string )
{
	_string = new _srep( (const char *)(zString &)string );
	assert( _string );
}
#endif // ZAPP



/*
#if defined(_WINDOWS_) || defined(INC_OLE2)
// Constructor for BSTR
cString::cString( const BSTR &string )
{
	if ( string == NULL )
	{
		_string = new _srep();
		assert( _string );
	}
	else
	{
		int length = WideCharToMultiByte(CP_ACP, 0, string, -1, NULL, 0, NULL, NULL);
		_string = new _srep( length+1 );
		assert( _string );
		WideCharToMultiByte(CP_ACP, 0, string, -1, *this, length+1, NULL, NULL);
	}
}
#endif //_WINDOWS_
*/


//
// Destructor.
//

cString::~cString()
{
	STRING_LOCK( _string )

	if ( --_string->number == 0 )
	{
		STRING_UNLOCK( _string )
		delete _string;
		_string = NULL;
	}
	else
	{
		STRING_UNLOCK( _string )
	}
}


// Return the current length of the string.
size_t
cString::length() const
{
	STRING_LOCK( _string )

	if ( (_string->cacheLength == 0) &&
		 (_string->string[0] != '\0') )
	{
		int len = strlen( (const char*)_string->string );
		_string->cacheLength=len;
	}
#ifndef NDEBUG
	else
	{
		size_t len = strlen( (const char*)_string->string );
		if (_string->cacheLength != len )
		{
			assert(!"cString::length() cache mismatch");
//			debug(dbg_error,"cache = " << _string->cacheLength <<
//					", strlen = " << len << '\n');
			_string->cacheLength=len; // Don't assert on same problem again.
		}
	}
#endif // DEBUG

	STRING_UNLOCK( _string )

	return _string->cacheLength;
}

//
// Detach the string, from all other "copies/attachments".
//

void
cString::_detach()
{
	assert( _string );

	// More then "this" is looking at this string.
	if ( _string->number > 1 )
	{
		STRING_LOCK( _string )

		// Create our string
		_srep *tempString = new _srep( length() + 1 );
		assert( tempString );

		// Copy the data.
		strcpy( (char*)tempString->string, (char*)_string->string );

		// Decrement the number of references looking at this string.
		_string->number--;

		// Copy the cached length.
		tempString->cacheLength = _string->cacheLength;

		STRING_UNLOCK( _string )

	#ifdef _MT
		// There must not be ANY locks outstanding before we reallocate the string
		assert( _string->_sect.LockCount == -1 );
	#endif
		// Point at the new string
		_string = tempString;
	}
}


//
// Resize the buffer. The string may be truncated to newBufferLength -1,
// if it is too long.
//

int
cString::bufferLength( size_t newBufferLength )
{
	assert( newBufferLength != 0 );

	// Detach this reference.
	_detach();

	// BAIL out if do not have to do any work dude.!!!!
	if ( _string->bufferLength == newBufferLength )
	{
		return newBufferLength;
	}

	STRING_LOCK( _string )

	// Create a new string.
	_srep *tempString = new _srep( newBufferLength );
	assert( tempString );

	// Copy the contents of _string into the new string.
	strncpy( (char*)tempString->string, (char*)_string->string, newBufferLength );
	tempString->string[newBufferLength-1] = 0;

	STRING_UNLOCK( _string )

	delete _string;
	_string = NULL;

	// Point at the "new" string.
	_string = tempString;

	// succcess.
	return 1;
}


//
// Append s to the string, making room as necessary.
//

void
cString::append( const char *s )
{
	assert( s );

	// Detach this reference.
	_detach();

	size_t currLen = length();
	size_t newLen = strlen(s);

	bufferLength( currLen + newLen + 1 );

	STRING_LOCK( _string )
	strcpy( (char*)_string->string + currLen, s );
	_string->cacheLength = newLen + currLen;
	STRING_UNLOCK( _string )
}


//
// Prepend s to the string, making room as necessary.
//

void
cString::prepend( const char *s )
{
	assert( s );

	// Detach this reference.
	_detach();

	size_t prepLen = strlen( s );
	size_t currLen = length();

	bufferLength( prepLen + currLen + 1 );

	STRING_LOCK( _string )
	memmove( (char*)_string->string + prepLen, (char*)_string->string, currLen + 1 );
	memcpy( (char*)_string->string, s, prepLen );
	_string->cacheLength = prepLen + currLen;
	STRING_UNLOCK( _string )
}


//
// Convert all upper-case characters in the string to lower-case.
//

void
cString::toLower()
{
	// Detach this reference.
	_detach();

	STRING_LOCK( _string )
	int len = length();
	for ( int i=0; i < len; i++ )
	{
		_string->string[i] = (char)tolower( _string->string[i] );
	}

	STRING_UNLOCK( _string )
}


//
// Convert all lower-case characters in the string to upper-case.
//

void
cString::toUpper()
{
	// Detach this reference.
	_detach();

	STRING_LOCK( _string )
	int len = length();
	for ( int i=0; i < len; i++ )
	{
		_string->string[i] = (char)toupper( _string->string[i] );
	}

	STRING_UNLOCK( _string )
}


//
// Convert first char to uppercase, and the rest to lower case.
//

void
cString::toTitle()
{
	// Detach this reference.
	_detach();

	STRING_LOCK( _string )
	int len = length();
	for ( int i=0; i < len; i++ )
	{
		// set the first char to uppercase
		if ( i == 0 )
		{
			_string->string[i] = (char)toupper( _string->string[i] );
		}
		else
		{
			// set the every other char to lowercase
			_string->string[i] = (char)tolower( _string->string[i] );
		}
	}

	STRING_UNLOCK( _string )
}


//
// Reverse the string.
//
void
cString::reverse()
{
	_detach(); 

	STRING_LOCK( _string )
	_strrev( (char*)_string->string );
	STRING_UNLOCK( _string )
}


//
// Return the index to the first occurrence of s in the string.
// Returns -1 if no match found.
//

int
cString::index( size_t start, const char *s, int caseSensitive ) const
{
	STRING_LOCK( _string )

	assert( s );
	assert( start <= length() );

	// Search for the first occurence of the first "s".
	char *tempString = 0;
	if ( caseSensitive )
	{
		tempString = strstr( (char*)_string->string + start, s );
	}
	else
	{
		int len = length();
		// stristr() does not exist, so we must simulate it here
		int	lengthSubString = strlen( s );
		for ( char *offset = (char*)_string->string + start;
			  offset < _string->string + len - (lengthSubString-1);
			  offset++ )
		{
			if ( _strnicmp( offset, s, lengthSubString ) == 0 )
			{
				tempString = offset;
				break;
			}
		}
	}
	long result = -1;
	if( tempString )
	{
		result = tempString - (const char *) _string->string;
	}

	STRING_UNLOCK( _string )
	return result;
}


//
// Return the index to the first occurrence of c in the string.
// Returns -1 if no match found.
//

int
cString::index( size_t start, char c, int caseSensitive ) const
{
	STRING_LOCK( _string )

	// Make sure start is sensible
	assert( start <= length() );

	// Search for the first occurence of the first "c".
	char *tempString = 0;
	if ( caseSensitive )
	{
		tempString = strchr( (char*)_string->string + start, c );
	}
	else
	{
		int len=length();
		// strichr() does not exist, so we must simulate it here
		for ( char *offset = (char*)_string->string + start;
			  offset < _string->string + len;
			  offset++ )
		{
			if ( tolower(*offset) == tolower(c) )
			{
				tempString = offset;
				break;
			}
		}
	}
	long result = -1;
	if( tempString )
	{
		result = tempString - (const char *) _string->string;
	}

	STRING_UNLOCK( _string )
	return result;
}


//
// Returns the Index of the right most string s in the internal string.
// If start is specified it indicates the rightmost index within the string at
// which the search string (s) may start.
//

int
cString::rIndex( size_t start, const char *s, int caseSensitive ) const
{
	STRING_LOCK( _string )

	assert( s );
	assert( start <= length() );

	// Search for the first occurence of the first "s".
	int	length = strlen( s );
	long result = -1;
	if ( caseSensitive )
	{
		for ( char *offset = (char*)_string->string + start;
			  offset >= _string->string;
			  offset-- )
		{
			if ( strncmp( offset, s, length ) == 0 )
			{
				result = offset - (const char *) _string->string;
				STRING_UNLOCK( _string )
				return result;
			}
		}
	}
	else
	{
		for ( char *offset = (char*)_string->string + start;
			  offset >= _string->string;
			  offset-- )
		{
			if ( _strnicmp( offset, s, length ) == 0 )
			{
				result = offset - (const char *) _string->string;
				STRING_UNLOCK( _string )
				return result;
			}
		}
	}

	STRING_UNLOCK( _string )
	return result;
}


//
// Returns the Index of the string s in the internal string.
//

int
cString::rIndex( size_t start, const char c, int caseSensitive ) const
{
	STRING_LOCK( _string )

	// Make sure start is sensible
	assert( start <= length() );

	long result = -1;
	// Search for the first occurence of the first "c".
	if ( caseSensitive )
	{
		for ( char *offset = (char*)_string->string + start;
				offset >= _string->string;
				offset-- )
		{
			if ( *offset == c )
			{
				result = offset - (const char *) _string->string;
				STRING_UNLOCK( _string )
				return result;
			}
		}
	}
	else
	{
		for ( char *offset = (char*)_string->string + start;
				offset >= _string->string;
				offset-- )
		{
			if ( tolower(*offset) == tolower(c) )
			{
				result = offset - (const char *) _string->string;
				STRING_UNLOCK( _string )
				return result;
			}
		}
	}

	STRING_UNLOCK( _string )
	return result;
}


//
// Returns the left n characters of the string. If the string is less than n characters long, the entire
// string will be returned.
//

cString
cString::leftString( size_t n ) const
{
	if ( n >= length() )
	{
		return *this;
	}

	// Create a cString.
	cString tempcString( n + 1 );

	STRING_LOCK( _string )
	// Copy the data across.
	strncpy( (char*)tempcString._string->string, (char*)_string->string, n );
	tempcString._string->string[n] = NULL;

	STRING_UNLOCK( _string )

	return tempcString;
}

//
// Replace the left n characters of the string. Replacement string can be a different length.
// If the string is less than n characters long, the entire string will be replaced.
//

void
cString::leftString( size_t n, const char *s )
{
	assert( s );

	// Detach this reference.
	_detach();

	// Optimize if attempting to replace all (or more) of the string
	size_t len_string = length();
	if ( n >= len_string )
	{
		*this = s;
		return;
	}
	size_t lenS = strlen( s );

	// Only resize the buffer if it needs to get bigger
	size_t	newLength = lenS + len_string - n + 1;
	size_t oldLength = bufferLength();
	if ( newLength > oldLength )
	{
		bufferLength( newLength );
	}

	STRING_LOCK( _string )
	// Copy the memory ( and NULL ) into the correct position.
	memmove( (char*)_string->string + lenS, (char*)_string->string + n, 1+len_string-n);
	memcpy( (char*)_string->string, s, lenS );
	_string->cacheLength=(newLength-1);

	STRING_UNLOCK( _string )
}


//
// Return a sub-string of the string. Attempts to go beyond the string will be clipped.
//

cString
cString::subString( size_t start, size_t lengthsub ) const
{
	STRING_LOCK( _string )

	size_t len = length();
	// Check to see if we are not trying to copy data which is not in our string.
	if ( start >= len )
	{
		start = len;
		lengthsub = 0;
	}
	if ( ( start + lengthsub ) >= len )
	{
		lengthsub = len - start;
	}

	cString tempcString( lengthsub + 1 );
	char *temp = (char*)_string->string + start;
	strncpy( (char*)tempcString._string->string, temp, lengthsub );
	tempcString._string->string[ lengthsub ] = 0;

	STRING_UNLOCK( _string )
	return tempcString;
}


//
// Replace a sub-string of the string. Attempts to go beyond the string will be clipped.
// Replacement string can be a different length.
//

void
cString::subString( size_t start, size_t len, const char *s)
{
	assert( s );

	// Detach this reference.
	_detach();

	size_t lenS = length();
	// Check to see if we are trying to copy data which is not in our string.
	if ( start >= lenS )
	{
		append( s );
		return;
	}
	// Check to see if we are trying to replace more chars then what we have got dude.!
	if ( ( start + len ) >= lenS )
	{
		len = lenS - start;
	}

	size_t lenRep = strlen( s );

	// Only resize the buffer if it needs to get bigger
	size_t	newLength = lenRep - len + lenS + 1;
	if ( newLength > bufferLength() )
	{
		bufferLength( newLength );
	}

	STRING_LOCK( _string )
	// Move the end part of our string to the correct place.
	memmove( (char*)_string->string + start + lenRep, (char*)_string->string + start + len, lenS - len - start + 1 );
	// Copy in the replacement string.
	memcpy( (char*)_string->string + start, (char*)s, lenRep );
	_string->cacheLength=0;

	STRING_UNLOCK( _string )
}


//
// Returns right n characters of the string.
// If the string is longer than n characters, the entire string will
// be returned.
//

cString
cString::rightString( size_t n ) const
{
	STRING_LOCK( _string )

	size_t len = length();
	// If they are attempting to get the whole String.
	if ( n > len )
	{
		STRING_UNLOCK( _string )
		return *this;
	}

	cString str( (char*)_string->string + len - n );

	STRING_UNLOCK( _string )
	return str;
}


//
// Format data values into the string in the style of printf.
//

cString &
cString::format( const char *fmt, ... )
{
	// Sanity
	assert( fmt != NULL );

	// Create the argument list.
	va_list	ap;
	va_start( ap, fmt );

	// Perform the sprintf.
	int result = vsprintf( *this, fmt, ap );

	// Close the argument list.
	va_end( ap );

	// Check to see if we did go over our memory limit.
	assert( result < (int)bufferLength() );

	return *this;
}


//
// Finds the first instance of the specified character 
// searching backwards from the specified position
// If the search fails the function returns -1
//

int
cString::findReverse( char c, int pos )
{
	STRING_LOCK( _string )

	assert ( (int)pos < (int)bufferLength() );
	if ((size_t)pos >= bufferLength() )
		pos = bufferLength()-1;

	// If pos is -1 (or just negative) it has defaulted and needs to be set here
	if (pos < 0)
		pos = bufferLength()-1;

	long result = -1;
	for ( int i = pos; i >= 0; i--)
	{
		size_t j = (size_t)i;
		assert( (size_t)i == j );

		if ( _string->string[j] == c )
		{
			result = j;
			STRING_UNLOCK( _string )
			return result;
		}
	}

	STRING_UNLOCK( _string )
	return result;
}

//
// Replace the right n characters of the string.
// Replacement string can be a different length. If the string is longer
// than n characters, the entire string will be replaced.
//

void
cString::rightString( size_t n, const char *s )
{
	assert( s );

	// Detach the string.
	_detach();

	size_t len = length();
	// If they are attempting to replace all ( or more ) of the string.
	if ( n >= len )
	{
		*this = s;
		return;
	}

	size_t lenRep = strlen( s );
	bufferLength( len - n + lenRep + 1 );
	STRING_LOCK( _string )
	strcpy( (char*)_string->string + len - n, (char*)s );
	_string->cacheLength= len-n+lenRep;
	STRING_UNLOCK( _string )
}


// Replace the first occurrence of s in the string with r
// Starts looking for the substring at the position given by start.
// Returns the position of the replacement or -1 if no replacement took place
int 
cString::replace( const char* s, const char* r, size_t start, int caseSensitive )
{
	// This could be way more efficient but this is easier
	assert( s );
	assert( r );
	
	size_t lenSub = strlen( s );

	if ( start > (length() - lenSub) )
	{
		return -1;
	}

	// Find the substring
	int subIndex = index( start, s, caseSensitive );

	if ( subIndex == -1 )
	{
		return -1;
	}
	
	// Replace the substring
	subString( subIndex, lenSub, r ); 

	return subIndex;
}


// Replaces all occurences of s in the string with r
// Returns the number of substitutions
int 
cString::replaceAll( const char* s, const char* r, int caseSensitive )
{
	// This could be way more efficient but this is easier
	int n = 0;
	size_t lenRep = strlen( r );
	int	index = replace( s, r, 0, caseSensitive );
	while ( index > -1 )
	{
		n++;
		index += lenRep;
		index = replace( s, r, index, caseSensitive );
	}

	return n;
}

#ifdef DEBUG_STRING_CMP
//
// A case sensitive string comparison.
// 	Returns (the same as strcmp)
// 			-ve	if *this < s
// 			0      *this == s
//  		+ve if *this > s
// This version is for debug purposes and is only called by the
// comparison operators.  This function calls cmp() to perform the
// comparison.  If the string isn't == to the passed-in string,
// icmp() is called and if the strings are ==, a debug is output
// to indicate that the caller used the == operator which is
// case-sensitive when they possibly meant to use the case-insensitive
// icmp() function.
//

int
cString::cmp_debug( const char *s ) const
{
	STRING_LOCK( _string )

	// Try the case-sensitive comparison.
	int result_cmp = cmp( s );
	if ( result_cmp == 0 )
	{
		return 0;
	}

	// Strings weren't equal with a case-sensitive comparison.  Let's see if
	// they are equal with a case-insensitive comparision.

	if ( icmp(s) == 0 )
	{
		// So, if the caller had have used icmp() they would have found that
		// the strings were equal.  However, since the cString == operator does
		// a cmp(), the user caller will get told that the strings are not
		// equal.  Let's alert them to this potential error.
		// debug( 0, "cString operator== used, maybe wanted icmp().  \"" << _string->string << "\", \"" << s << "\"\n" );
	}

	STRING_UNLOCK( _string )

	// No matter what we found, we want this function to behave as cmp() does.
	return result_cmp;
}
#endif // DEBUG_STRING_CMP


//
// Remove the leading 'c's.
//

void
cString::stripLeading( char c )
{
	assert( c != NULL );

	// Detach this reference.
	_detach();

	int i = 0;
	while ( _string->string[i] == c )
	{
		i++;
	}
	// If we stripped some dude!!.
	if ( i > 0 )
	{
		STRING_LOCK( _string )
		size_t len=length();
		memmove( (char*)_string->string, (char*)_string->string + i, len - i + 1 );
		_string->cacheLength=len - i;
		STRING_UNLOCK( _string )
	}
}


//
// Strip all leading characters which are present in set.
//

void
cString::stripLeading(const char *set)
{
	assert( set );

	// Detach this reference.
	_detach();

	STRING_LOCK( _string )
	int i = 0;
	while ( strchr( set, _string->string[i] ) )
	{
		i++;
	}
	if ( i > 0 )
	{
		size_t len=length();
		memmove( (char*)_string->string, (char*)_string->string + i, len - i + 1 );
		_string->cacheLength=len - i;
	}

	STRING_UNLOCK( _string )
}


//
// Remove the leading white-space. If isspace() says it's white-space, its white-space.
//

void
cString::stripLeadingWhite()
{
	// Detach this reference.
	_detach();

	STRING_LOCK( _string )
	int i = 0;
	while ( isspace( _string->string[i] ) )
	{
		i++;
	}
	if ( i > 0 )
	{
		size_t len=length();
		memmove( (char*)_string->string, (char*)_string->string + i, len - i + 1 );
		_string->cacheLength=len - i;
	}

	STRING_UNLOCK( _string )
}


//
// Remove the trailing 'c's. (And suck up the vacated space).
//

void
cString::stripTrailing( char c )
{
	assert( c != NULL );

	// Detach this reference.
	_detach();

	STRING_LOCK( _string )
	size_t i = 0;
	size_t len = length();
	while ( ( _string->string[ len -1 - i] == c ) && ( i < len ) )
	{
		i ++;
	}
	// If we stripped some dude!!.
	if ( i > 0 )
	{
		_string->string[ len - i ] = 0;
	}
	_string->cacheLength=len - i;

	STRING_UNLOCK( _string )
}


//
// Strip all trailing characters which are present in set.
//

void
cString::stripTrailing( const char *s )
{
	assert( s );

	// Detach this reference.
	_detach();

	STRING_LOCK( _string )
	size_t i = 0;
	size_t len = length();
	while ( strchr( s, _string->string[ len -1 - i] ) && ( i < len ) )
	{
		i ++;
	}
	// If we stripped some dude!!.
	if ( i > 0 )
	{
		_string->string[ len - i ] = 0;
	}
	_string->cacheLength=len - i;

	STRING_UNLOCK( _string )
}


//
// Remove the trailing white-space.
// If isspace() says it's white-space, its white-space.
//

void
cString::stripTrailingWhite()
{
	// Detach this reference.
	_detach();

	STRING_LOCK( _string )
	size_t i = 0;
	size_t len = length();
	while ( ( i < len ) && isspace( _string->string[ len-1 - i] ) )
	{
		i ++;
	}
	// If we stripped some dude!!.
	if ( i > 0 )
	{
		_string->string[ len - i ] = 0;
	}
	_string->cacheLength=len - i;

	STRING_UNLOCK( _string )
}


//
// Collapse sequences of white-space to single space characters.
// eg. " \t hello  there \t xxx  " becomes " hello there xxx "
//

void
cString::collapseWhite()
{
	STRING_LOCK( _string )

	// Maintain a length variable, len, to improve efficiency
	int len = length();
	int start = 0;	// start of whitespace sequence
	while ( start < len )
	{
		// Skip non-whitespace characters
		for ( ; start<len && !isspace(_string->string[start]); start++ )
		{
			// do nothing
		}
		if ( start == len )
		{
			STRING_UNLOCK( _string )
			return;
		}
		// start is at the start of a whitespace sequence, now find the end
		int end;
		for ( end=start; end+1<len && isspace(_string->string[end+1]); end++ )
		{
			// do nothing
		}
		// Collapse whitespace sequence to a single space.  Even if there is
		// only one whitespace character it may be a tab so collapse always.
		STRING_UNLOCK( _string )
		subString( start, end-start+1, " " );
		STRING_LOCK( _string )
		// Adjust length variable, len
		len -= end-start;
		// Adjust start
		start++;
	}
	_string->cacheLength=len;

	STRING_UNLOCK( _string )
}


//
// Output the string to a stream.
//

/*
int
cString::output( ostream &out ) const
{
	STRING_LOCK( _string )
	out << (char*)_string->string;
	STRING_UNLOCK( _string )
	return 1;
}


//
// Input a string from a stream.
//

int
cString::input( istream &in, char terminator, int eatTerminator )
{
	char temp[2];
	temp[0] = terminator;
	temp[1] = 0;
	return input( in, temp, eatTerminator );
}


//
// Input a string from a stream.
//

int
cString::input( istream &in, const char *terminatorSet, int eatTerminator )
{
	// Clear the string ready for use
	_detach();
	
	STRING_LOCK( _string )
	_string->string[0] = 0;
	_string->cacheLength = 0;

	const size_t buffSize = 255;
	char  string[buffSize];
	int i = 0;

	int c = in.get();
	while ( c != EOF && c != 0 )
	{
		if ( terminatorSet == NULL )
		{
			if ( isspace( c ) )
			{
				if ( !eatTerminator )
				{
					in.putback((char)c);
				}
				break;
			}
		}
		else if ( strchr( terminatorSet, c ) )
		{
			if ( !eatTerminator )
			{
				in.putback((char)c);
			}
			break;
		}
		string[i++] = (char)c;
		if ( i == ( buffSize - 1 ) )
		{
			string[i] = 0;
			append( string );
			i = 0;
		}
		c = in.get();
	}
	string[i] = 0;
	append( string );

	STRING_UNLOCK( _string )
	return 1;
}
*/


//
// Return non-zero if the string is composed entirely of digits
//

int
cString::isDigits() const
{
	STRING_LOCK( _string )

	for ( int i = 0; _string->string[i]; i++ )
	{
		// As soon as we find a non-digit, we can stop
		if ( !isdigit( _string->string[i] ) )
		{
			STRING_UNLOCK( _string )
			return 0;
		}
	}

	STRING_UNLOCK( _string )
	return 1;
}


// returns true if the string is alphabetic
bool
cString::isAlpha() const
{
	STRING_LOCK( _string )

	for ( int i = 0; _string->string[i]; i++ )
	{
		// As soon as we find a non-alpha, we can stop
		if ( !isalpha( _string->string[i] ) )
		{
			STRING_UNLOCK( _string )
			return false;
		}
	}


	STRING_UNLOCK( _string )
	return true;
}

// returns true if the string is alphanumeric
bool
cString::isAlNum() const
{
	STRING_LOCK( _string )

	for ( int i = 0; _string->string[i]; i++ )
	{
		// As soon as we find a non-alnum, we can stop
		if ( !isalnum( _string->string[i] ) )
		{
			STRING_UNLOCK( _string )
			return false;
		}
	}

	STRING_UNLOCK( _string )
	return true;
}



//
// Set the string to an ASCII representation of the given value
//

void
cString::setInt( int value )
{
	sprintf( *this, "%d", value );
}


//
// Set the string to an ASCII representation of the given value
//

void
cString::setLong( long value )
{
	sprintf( *this, "%ld", value );
}


//
// Convert the string into a number of the given type
//

int
cString::getInt() const
{
	return atoi( *this );
}


//
// Convert the string into a number of the given type
//

long
cString::getLong() const
{
	return atol( *this );
}


//
// Straightforward assignment. This is different to the
// const char * assignment operator as it needs to update
// the reference counting.
//

cString&
cString::operator=(const cString &s)
{
	if ( s._string == _string )
	{
		// BAIL out dude, we have the same string.
		return *this;
	}

	STRING_LOCK( s._string )

	s._string->number++;
	if ( --_string->number == 0 )
	{
		delete _string;
		_string = NULL;
	}

	_string = s._string;

	STRING_UNLOCK( s._string )

	return *this;
}


//
// Straightforward assignment. As with the constructor,
// the cString will take a copy of the input string.
//

cString&
cString::operator=(const char *s)
{
	STRING_LOCK( _string )

	assert( s );

	if ( --_string->number == 0 )
	{
		STRING_UNLOCK( _string )
		
		delete _string;
		_string = NULL;
	}
	else
	{
		STRING_UNLOCK( _string )
	}

	_string = new _srep( s );
	assert( _string );

	return *this;
}


//
// Straightforward assignment. Only available if compiled
// with zApp.
// The cString class must compile without zApp.
//

#ifdef ZAPP
cString&
cString::operator=(const zString &s)
{
	return operator=( (const char *) (zString &) s );
}
#endif ZAPP


//
// Return a cString which is a concatination of this and s.
//

cString
cString::operator+(const char *s) const
{
	assert( s );

	// Create a temp string with the data alreadiny in it.
	cString returnString = *this;
	// Append the new data
	returnString.append( s );

	return returnString;
}


//
// Append a character.
//

cString &
cString::operator+=( char c )
{
	// Detach this string
	_detach();

	operator[]( length() ) = c;

	return *this;
}


//
// Append a string.
//

cString &
cString::operator+=(const char *s)
{
	assert( s );

	// Detach this string
	_detach();

	append( s );

	return *this;
}


//
// Read a character of the string.
// If access beyond the end of the buffer is attempted, a NULL
// will be returned.
//

char
cString::operator[](size_t index) const
{
	STRING_LOCK( _string )

	// Check to see if we are looking at a valid character.
	if ( index >= _string->bufferLength )
	{
		STRING_UNLOCK( _string )
		return NULL;
	}

	// Return the reference to the char.
	char ch = _string->string[index];

	STRING_UNLOCK( _string )

	return ch;
}


//
// Modify a character of the string.
// The reference is only guaranteed until the next non-const
// operation on this string. The NULL at the end of the string
// is automatic. (Though jamming in a NULL before the final
// NULL will effectively shorten the string). If access beyond
// the end of the buffer is attempted, the buffer will be
// extended.
//

char &
cString::operator[](size_t index)
{
	// Detach this string
	_detach();

	// Make sure our string is big enough!!.
	if ( _string->bufferLength <= index + 1)
	{
		bufferLength( index + 2 );
	}
	else if ( index >= length() )
	{
		_string->string[index+1] = 0;
	}
	_string->cacheLength=0;

	return ( (char)_string->string[index] );
}

//
// Provide access to the interal buffer. This pointer is only guaranteed until
// the next time a non-const operation is performed on the string.
//
char *
cString::rawAccess()
{
	// Detach this string
	_detach(); 

	STRING_LOCK( _string )
	_string->cacheLength=0;
	STRING_UNLOCK( _string )

	return (char*)_string->string;
}


//
// Identical to the normal C functions of the same names, with the
// exception that the dst string will be automatically sized to be big
// enough. (And that cStrings can never be unterminated, and the usual
// version of strncpy can produce unterminated strings).
//

char *
strncat(cString &dst, const char *src, size_t len)
{
	assert( src );

	size_t lend = dst.length();

	dst.bufferLength(lend+len+1);
	strncat(dst.rawAccess(),src,len);
	(dst.rawAccess())[lend+len]='\0';

	return  dst.rawAccess();
}


char *
strncpy(cString &dst, const char *src, size_t len)
{
	assert( src );

	dst.bufferLength(len+1);
	strncpy(dst.rawAccess(), src, len);
	(dst.rawAccess())[len]='\0';

	return dst.rawAccess();
}


//
// Identical to the normal sprintf, except that the dst string will
// be automatically be sized to be big enough for the result.
//

int
sprintf(cString &dst, const char *fmt, ...)
{
	assert( fmt );

	// Create the argument list.
	va_list ap;
	va_start( ap, fmt );

	// Perform the sprintf.
	int result = vsprintf( dst, fmt, ap );

	// Close the argument list.
	va_end( ap );

	// Check to see if we did go over our memory limit.
	assert( result < (int)dst.bufferLength() );

	return result;
}


//
// Identical to the normal sprintf, except that the dst string will
// be automatically be sized to be big enough for the result.
//

int
vsprintf(cString &dst, const char *fmt, va_list arglist)
{
	assert( fmt );

//#pragma message NOTE("vsprintf assumes a maximum result of 1024 if the buffer is not set otherwise - VERY VERY BAD")
	int resize = 0;
	// Size our buffer up to 1024 bytes (min) and free any unused after the sprintf.
	if ( dst.bufferLength() < 1024 )
	{
		resize = 1;
		dst.bufferLength( 1024 );
	}
	// Perform the sprintf.
	int result = vsprintf( dst.rawAccess(), fmt, arglist );

	// Check to see if we did go over our memory limit.
	assert( result < (int)dst.bufferLength() );
	// Go back to the min that we can.
	if ( resize )
	{
		dst.bufferLength( dst.length() + 1 );
	}

	return result;
}

//
// Returns a naive (non-HJ-based) count of the words in takeText.
//
long
cString::wordCount() const
{
	// No need to work out empty strings.
	if( isEmpty() )
	{
		return 0;
	}

	STRING_LOCK( _string )

	// Consider a word to be a set of printable characters
	// ending in whitespace.
	long wordCount = 0;
	long textEnd = length();
	for( long c = 1; c <= textEnd; ++c )
	{
		// Look at this character and the one before it.
		char prevChar = _string->string[ ( size_t )( c - 1 ) ];
		char thisChar = _string->string[ ( size_t )c ];

		// Bump the word count if we're at the end of a word.
		if( ( ! isspace( prevChar ) ) &&
				isspace( thisChar )   &&
				isprint( prevChar ) )
		{
			++ wordCount;
		}
	}

	// Also add to the count if there's a word at the end of
	// the selection - it won't have been picked up in the loop.
	char lastChar = _string->string[ ( size_t ) textEnd - 1 ];
	if( isprint( lastChar ) && ! isspace( lastChar ) )
	{
		++ wordCount;
	}

	STRING_UNLOCK( _string )
	return wordCount;
}


