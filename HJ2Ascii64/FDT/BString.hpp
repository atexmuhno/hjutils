
// fdt/bstring.hpp - BSTR string wrapper
// Copyright 1999 Cybergraphic Systems.  All rights reserved
//
//
// Usage:
//
// cBString and cSimpleBString are used to wrap the COM data type BSTR, which
// has special functions that allocate and free BSTR data. The two classes
// differ in the concept of "ownership" of the contained BSTR, and whether or
// not to free the contained BSTR on destruction.
//
// cBString will free the contained BSTR on destruction.
// This is used in two contexts:
// -	to wrap an BSTR that was constructed elsewhere, so as to cast it to cString
//		(or char *) and to free the BSTR when the cBString destructs.
// -	to dynamically create a new temporary BSTR from a cString (or char *)
//		within the parameters of a function call requiring a BSTR argument, so
//		that the BSTR is freed after the function returns.
//
// cSimpleBString will not free the contained BSTR on destruction
// This is used in two contexts:
// -	to wrap an existing BSTR so as to cast it to cString (or char *) but not
//		to free the BSTR when the cSimpleBString desctructs (BSTR "in" arguments).
// -	to create a new BSTR from a cString (or char *) and return it but not
//		to free the BSTR when the cSimpleBString destructs (BSTR "out" arguments).
//
//
// For calling COM methods that have BSTR arguments, use cBString
//
//		cString strIn("Hello");
//		BSTR retBSTR;
//		// ComMethodCall([in] BSTR, [out] &BSTR)
//		err = p->ComMethodCall( cBString(strIn), &retBSTR);
//		cString strOut = cBString(retBSTR);
//
// For implementing methods that have BSTR arguments, use cSimpleBString
//
//		STDMETHODIMP CMyComObject::ComMethodCall(BSTR strIn, BSTR *strOut)
//		{
//			cString tempString = cSimpleBString(strIn);
//			cString retString = DoSomething(tempString);
//			*strOut = cSimpleBString(retString);
//			return S_OK;
//		}
//

#ifndef FDT_BSTRING_HPP
#define FDT_BSTRING_HPP

#include <ole2.h>
#include "string.hpp"


class cSimpleBString
{
public:
	cSimpleBString();
   		// default constructor

	cSimpleBString( BSTR bstr );
		// constructing with BSTR will not free the BSTR on destruction
		// since cSimpleBString does not own nor free the BSTR.

	cSimpleBString( const cSimpleBString &bstr );
        // copy constructor causes copy of BSTR without new allocation
        // since cSimpleBString does not own nor free the BSTR.

    cSimpleBString( const char *str );
        // construction with 'const char *' causes allocation of new BSTR.
        // this is so cSimpleBString can be used to allocate a new BSTR
        // and return this through a return argument without freeing the BSTR
        // on cSimpleBString desctruction.

	cSimpleBString( const cString &str );
        // construction with cString causes allocation of new BSTR.
        // this is so cSimpleBString can be used to allocate a new BSTR
        // and return this through a return argument without freeing the BSTR
        // on cSimpleBString desctruction.

	virtual ~cSimpleBString();
        // destructor of cSimpleBString does not free the BSTR.
        // derived cBString destructor will free the BSTR.

	operator BSTR() const;
		// returns the BSTR.

	operator cString() const;
		// converts the BSTR into a cString.

	BSTR *operator &();
		// Return a pointer to the internal BSTR. This is only valid if the
		// internal BSTR is NULL. The purpose of this operator is for passing
		// new cSimpleBStrings into COM functions which return a BSTR via a
		// parameter.

	cSimpleBString& operator= ( const cSimpleBString &bstr );
        // assignment causes copy of BSTR without new allocation
        // since cSimpleBString does not own nor free the BSTR.

	bool operator== ( const cSimpleBString &bstr );
		// case sensitive comparison

	bool operator!= ( const cSimpleBString &bstr );
		// case sensitive comparison

protected:
	BSTR _getBSTR(void) const;
		// return the contained BSTR.

    void _freeBSTR(void);
		// free the contained BSTR.

    void _assignNewBSTR(const char *str);
        // converts the string into a wide-char string, allocates
        // a new BSTR and assigns this to the contained BSTR member.

    void _assignNewBSTR(BSTR bstr);
		// allocates a new BSTR and assigns this to the contained BSTR member.

private:
	BSTR _bstr;

};

class cBString : public cSimpleBString
{
public:
	cBString();
		// default constructor

	cBString( BSTR bstr );
		// cBString will own and free the contained BSTR.

    cBString( const cBString &bstr );
        // copy constructor causes free of contained BSTR, allocation of
        // new BSTR and assiging this to BSTR member. cBString destructor
        // will free the contained BSTR.

   	cBString( const char *str );
        // construction with 'const char *' causes allocation of new BSTR.
        // cBString will free the contained BSTR on destruction.

	cBString( const cString &str );
        // construction with cString causes allocation of new BSTR.
        // cBString will free the contained BSTR on destruction.

    virtual ~cBString();
        // destructor will free the contained BSTR because cBString
        // is considered to own the BSTR and is responsible for freeing the
        // BSTR. The cSimpleBString desctructor does not free the contained BSTR
        // so that cSimpleBString can be used to allocate a new BSTR but transfer
        // ownership of the BSTR and the responsibility of freeing the BSTR.

    cBString& operator= ( const cBString &bstr );
        // assignment operator causes free of contained BSTR, allocation of
        // new BSTR and assiging this to BSTR member. cBString destructor
        // will free the contained BSTR.

};

#endif // FDT_BSTRING_HPP

