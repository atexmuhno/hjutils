// cString - string - fdt/string.hpp
/* Copyright 1996, 1997 Cybergraphic Systems.  All rights reserved. */


// WARNING - This class is thread-safe.
// New members should use the STRING_LOCK / UNLOCK macros (whether
// compiling in a multi-threaded environment or not) whenever the
// class is being modified.
// Any members which call _detach() should use the STRING_LOCK
// macro first, and ensure that STRING_UNLOCK is used before 
// the function exits.

#ifndef FDT_STRING_HPP
#define FDT_STRING_HPP

#ifndef USE_LIBRARY_HPP
	#include <stdio.h>
//	#include <iostream.h>
	#include <stdarg.h>
	#include <string.h>
	#include <ctype.h>
	#include <assert.h>
	#ifdef ZAPP
		#include <zapp.hpp>
	#endif //ZAPP
	#ifdef _MT
		#include <windows.h>
	#endif // _MT
#endif // USE_LIBRARY_HPP

#ifdef _MT
	#define STRING_LOCK( str )		{ assert( str ); ::EnterCriticalSection( &str->_sect ); }
	#define STRING_UNLOCK( str )	{ assert( str ); ::LeaveCriticalSection( &str->_sect ); }
#else
	#define STRING_LOCK( str )
	#define STRING_UNLOCK( str )
#endif // _MT

#define _CRT_SECURE_NO_DEPRECATE 1

// Enable/disable the following lines for debugging calls to == operator.
//#ifndef NDEBUG
//#define DEBUG_STRING_CMP
//#endif // NDEBUG

/*
 *	A reference counted string class.
 *	Since it is reference counted, it is quite efficient to copy a cString (both
 *	for time and space). Users of cString must be careful to use const-reference
 *	parameters where appropriate so that the reference counting system will work
 *	efficiently.
 *
 *	See "The C++ Programming Language, Second Edition", page 248, by Bjarne
 *	Stroustrup for a basic reference counted string class.
 *
 *	A cString is guaranteed to maintain all data before the first NULL. If
 *	you poke directly into the buffer with additional NULLs, the data (and indeed
 *	the memory) after the first NULL is not guaranteed.
 *
 *	Note that most of the member functions take a "const char *" when they could
 *	have taken a "const String &". This is because the compiler will automatically
 *	(and efficiently) convert a "const String &" into a "const char *", whereas the
 *	conversion in the other direction would be less efficient.
*/

class cString {

public:

	//
	// Constructors
	//

	cString();
			// Construct an empty string.

	cString( const char *string );
			// Construct a cString from a normal C-String. The cString takes a copy.

	cString( const cString &string );
			// Copy constructor. This is different to the const char * constructor
			// as it needs to update the reference counts.

	explicit cString( size_t bufSize );
			// Construct an empty string with a bufSize initial bytes.

#ifdef ZAPP
	cString( const zString &string );
			// Construct a cString from a zApp zString. Only available if compiled
			// with zApp.
			// The cString class *must* compile without zApp.
#endif // ZAPP


/*#if defined(_WINDOWS_) || defined(INC_OLE2)
	cString( const BSTR &string );
			// Construct a cString from an OLE BSTR.
			// The cString class *must* compile without OLE/Windows.
#endif // _WINDOWS_*/



	cString::~cString();
			// Destructor

	//
	// Member Functions
	//

	size_t	length() const ; 
			// Return the current length of the string.

	bool	isEmpty() const { assert( _string ); return _string->string[0] == 0; }
			// Returns true if the string is empty (has a length of 0).

	size_t	bufferLength() const { assert( _string ); return _string->bufferLength; }
			// Return the current size of the string buffer.

	int		bufferLength( size_t newBufferLength );
			// Resize the buffer. The string may be truncated to newBufferLength -1,
			// if it is too long.
			// Returns 0 if the allocation of the new string fails, otherwise returns TRUE;

	void    append( const char *s );
	void    append( const cString &s ) { cString t( s ); append( (const char *) t ); }
			// Append s to the string, making room as necessary.

	void    prepend( const char *s );
	void    prepend( const cString &s ) { cString t( s ); prepend( (const char *) t ); }
			// Prepend s to the string, making room as necessary.

	void	toLower();
			// Convert all upper-case characters in the string to lower-case.

	void	toUpper();
			// Convert all lower-case characters in the string to upper-case.

	void	toTitle();
			// Convert first char to uppercase, and the rest to lower case.

	void	reverse();
			// Reverse the string.

	int		index( const char *s, int caseSensitive=1 ) const { return index( 0, s, caseSensitive ); }
	int		index( size_t start, const char *s, int caseSensitive=1 ) const;
			// Return the index to the first occurrence of s in the string.
			// Returns -1 if no match found.

	int		index( char c, int caseSensitive=1 ) const { return index( 0, c, caseSensitive ); }
	int		index( size_t start, char c, int caseSensitive=1 ) const;
			// Return the index to the first occurrence of c in the string.
			// Returns -1 if no match found.

	int		rIndex( const char *s, int caseSensitive=1 ) const { return rIndex( length(), s, caseSensitive ); }
	int		rIndex( size_t start, const char *s, int caseSensitive=1 ) const;
			// Return the index to the first occurrence of s in the string.
			// If start is specified it indicates the rightmost index within the
			// string at which the search string (s) may start.
			// Returns -1 if no match found.

	int		rIndex( const char c, int caseSensitive=1 ) const { return rIndex( length(), c, caseSensitive ); }
	int		rIndex( size_t start, const char c, int caseSensitive=1 ) const;
			// Return the index to the first occurrence of c in the string.
			// Returns -1 if no match found.

	cString leftString( size_t n ) const;
			// Returns the left n characters of the string. If the string is less than n characters long, the entire
			// string will be returned.

	void leftString( size_t n, const char *s );
			// Replace the left n characters of the string. Replacement string can be a different length.
			// If the string is less than n characters long, the entire string will be replaced.

	cString subString( size_t start, size_t length)const;
			// Return a sub-string of the string. Attempts to go beyond the string will be clipped.

	void subString( size_t start, size_t length, const char *s);
			// Replace a sub-string of the string. Attempts to go beyond the string will be clipped.
			// Replacement string can be a different length.

	cString rightString( size_t n ) const;
			// Returns right n characters of the string.
			// If the string is longer than n character, the entire string will
			// be returned.

	void rightString( size_t n, const char *s );
			// Replace the right n characters of the string.
			// Replacement string can be a different length. If the string is longer
			// than n character, the entire string will be replaced.

	cString &format( const char *fmt, ... );
			/**< Format data values into the string in the style of printf.
			 * Warning: The string will size itself to 1024 characters for the
			 * formatting operation. If more characters than this are generated
			 * by the format, a memory overwrite will occur!
			 */

	int replace( const char* s, const char* r, size_t start = 0, int caseSensitive=1 );
			// Replace the first occurrence of s in the string with r
			// Starts looking for the substring at the position given by start.
			// Returns the position of the replacement or -1 if no replacement took place

	int replaceAll( const char* s, const char* r, int caseSensitive=1 );
			// Replaces all occurences of s in the string with r
			// Returns the number of substitutions

	int  findReverse( char c, int pos = -1);
			// Finds the first instance of the specified character 
			// searching backwards from the specified position
			// If the search fails the function returns -1

	long wordCount() const;
			// Returns a naive (non-HJ-based) count of the words in our string.

	int		cmp( const char *s ) const { assert( _string ); return ( strcmp( (const char*)_string->string, s ) ); }
			// A case sensitive string comparison.
			// 	Returns (the same as strcmp)
			// 			-ve	if *this < s
			// 			0      *this == s
			//  		+ve if *this > s

	int		icmp( const char *s ) const { assert( _string ); return ( _stricmp( (const char*)_string->string, s ) ); }
			// A case insensitive string comparison. Returns (the same as stricmp)
			//			-ve	if *this < s
			//			0      *this == s
			//			+ve if *this > s

#ifdef DEBUG_STRING_CMP
	int		cmp_debug( const char *s ) const;
			// A case sensitive string comparison.
			// 	Returns (the same as strcmp)
			// 			-ve	if *this < s
			// 			0      *this == s
			//  		+ve if *this > s
			// This version is for debug purposes and is only called by the
			// comparison operators.  This function calls cmp() to perform the
			// comparison.  If the string isn't == to the passed-in string,
			// icmp() is called and if the strings are ==, a debug is output
			// to indicate that the caller used the == operator which is
			// case-sensitive when they possibly meant to use the case-insensitive
			// icmp() function.
#endif // DEBUG_STRING_CMP

	void	stripLeadingAndTrailing( char c ) { stripLeading( c ); stripTrailing( c ); }
			// Remove the leading and trailing 'c's. (And the vacated space)

	void	stripLeadingAndTrailingSpaces() { stripLeadingAndTrailing( ' ' ); }
			// Remove the leading and trailing spaces (spaces only)

	void	stripLeading( char c );
			// Remove the leading 'c's.

	void	stripLeading(const char *set);
			// Strip all leading characters which are present in set.

	void	stripLeadingSpaces() { stripLeading( ' ' ); }
			// Remove the leading spaces (spaces only).

	void	stripLeadingWhite();
			// Remove the leading white-space. If iswhite() says it's white-space, its white-space.

	void	stripTrailing( char c );
			// Remove the trailing 'c's. (And suck up the vacated space).

	void	stripTrailing( const char *s );
			// Strip all trailing characters which are present in set.

	void	stripTrailingSpaces() { stripTrailing( ' ' ); }
			// Remove the trailing spaces (spaces only).

	void	stripTrailingWhite();
			// Remove the trailing white-space.
			// If iswhite() says it's white-space, its white-space.

	void	collapseWhite();
			// Collapse sequences of white-space to single space characters.
			// eg. " \t hello  there \t xxx  " becomes " hello there xxx "

//	int		output( ostream &out )const;
			// Output the string to a stream.

//	int	  	input( istream &in, char terminator, int eatTerminator=0 );
//	int	  	input( istream &in, const char *terminatorSet = NULL, int eatTerminator=0 );
			// Input a string from a stream.
            // If the terminator is not specified it is NULL.

	//
	// Conversion
	//

	int		isDigits() const;
			// Return non-zero if the string is composed entirely of digits

	bool	isAlpha() const;
			// returns true if the string is alphabetic

	bool	isAlNum() const;
			// returns true if the string is alphanumeric

	void	setInt( int value );
	void	setLong( long value );
			// Set the string to an ASCII representation of the given value

	int		getInt() const;
	long	getLong() const;
			// Convert the string into a number of the given type

	//
	// Operators
	//

	// Assignment

	cString& operator=(const cString &s);
			// Straightforward assignment. This is different to the
			// const char * assignment operator as it needs to update
			// the reference counting.

	cString& operator=(const char *s);
			// Straightforward assignment. As with the constructor,
			// the cString will take a copy of the input string.
#ifdef ZAPP
	cString& operator=(const zString &s);
			// Straightforward assignment. Only available if compiled
			// with zApp.
			// The cString class must compile without zApp.
#endif ZAPP

	// Comparison
#ifdef DEBUG_STRING_CMP
	bool operator< (const cString &s) const { return ( cmp_debug( s ) < 0 ); }
	bool operator<=(const cString &s) const { return ( cmp_debug( s ) <= 0 ); }
	bool operator==(const cString &s) const { return ( cmp_debug( s ) == 0 ); }
	bool operator!=(const cString &s) const { return ( cmp_debug( s ) != 0 ); }
	bool operator>=(const cString &s) const { return ( cmp_debug( s ) >= 0 ); }
	bool operator> (const cString &s) const { return ( cmp_debug( s ) > 0 ); }

	bool operator< (const char *s) const { return ( cmp_debug( s ) < 0 ); }
	bool operator<=(const char *s) const { return ( cmp_debug( s ) <= 0 ); }
	bool operator==(const char *s) const { return ( cmp_debug( s ) == 0 ); }
	bool operator!=(const char *s) const { return ( cmp_debug( s ) != 0 ); }
	bool operator>=(const char *s) const { return ( cmp_debug( s ) >= 0 ); }
	bool operator> (const char *s) const { return ( cmp_debug( s ) > 0 ); }
	friend bool operator< (const char *a, const cString &b) { return ( b.cmp_debug( a ) < 0 ); }
	friend bool operator<=(const char *a, const cString &b) { return ( b.cmp_debug( a ) <= 0 ); }
	friend bool operator==(const char *a, const cString &b) { return ( b.cmp_debug( a ) == 0 ); }
	friend bool operator!=(const char *a, const cString &b) { return ( b.cmp_debug( a ) != 0 ); }
	friend bool operator>=(const char *a, const cString &b) { return ( b.cmp_debug( a ) >= 0 ); }
	friend bool operator> (const char *a, const cString &b) { return ( b.cmp_debug( a ) > 0 ); }
#else // DEBUG_STRING_CMP
	bool operator< (const cString &s) const { return ( cmp( s ) < 0 ); }
	bool operator<=(const cString &s) const { return ( cmp( s ) <= 0 ); }
	bool operator==(const cString &s) const { return ( cmp( s ) == 0 ); }
	bool operator!=(const cString &s) const { return ( cmp( s ) != 0 ); }
	bool operator>=(const cString &s) const { return ( cmp( s ) >= 0 ); }
	bool operator> (const cString &s) const { return ( cmp( s ) > 0 ); }

	bool operator< (const char *s) const { return ( cmp( s ) < 0 ); }
	bool operator<=(const char *s) const { return ( cmp( s ) <= 0 ); }
	bool operator==(const char *s) const { return ( cmp( s ) == 0 ); }
	bool operator!=(const char *s) const { return ( cmp( s ) != 0 ); }
	bool operator>=(const char *s) const { return ( cmp( s ) >= 0 ); }
	bool operator> (const char *s) const { return ( cmp( s ) > 0 ); }
	friend bool operator< (const char *a, const cString &b) { return ( strcmp( a, b ) < 0 ); }
	friend bool operator<=(const char *a, const cString &b) { return ( strcmp( a, b ) <= 0 ); }
	friend bool operator==(const char *a, const cString &b) { return ( strcmp( a, b ) == 0 ); }
	friend bool operator!=(const char *a, const cString &b) { return ( strcmp( a, b ) != 0 ); }
	friend bool operator>=(const char *a, const cString &b) { return ( strcmp( a, b ) >= 0 ); }
	friend bool operator> (const char *a, const cString &b) { return ( strcmp( a, b ) > 0 ); }
#endif // DEBUG_STRING_CMP

	// Concatenation
	cString operator+(const char    *s) const;
	cString operator+(const cString &s) const { return ( operator+( (const char *)s ) ); }
			// Return a cString which is a concatenation of this and s.

	cString &operator+=( char c );
			// Append a character.

	cString &operator+=( const char *s );
	cString &operator+=( const cString &s ) { cString t( s ); return operator+=( (const char *) t ); }
			// Append a string.

	// Indexing

	char operator[](size_t index) const;
			// Read a character of the string. The reference is only
			// guaranteed until the next non-const operation on this string.
			// If access beyond the end of the buffer is attempted, a NULL
			// will be returned.

	char &operator[](size_t index);
			// Modify a character of the string.
			// The reference is only guaranteed until the next non-const
			// operation on this string. The NULL at the end of the string
			// is automatic. (Though jamming in a NULL before the final
			// NULL will effectively shorten the string). If access beyond
			// the end of the buffer is attempted, the buffer will be
			// extended.

#ifdef _MSC_VER
	char operator[](int index) const { return operator[]( (size_t)index ); }
	char &operator[](int index) { return operator[]( (size_t)index ); }
#endif // _MSC_VER

	// Cast
	operator const char *() const { assert( _string ); return ( (const char*)_string->string ); }
			// Provide read-only access to the interal buffer.
			// This pointer is only guaranteed until the next time
			// a non-const operation is performed on the string.

#if ! defined( _MSC_VER ) || (_MSC_VER < 1200)
	operator char *() { assert( _string ); return rawAccess(); }
		// For Auld-Lang Syne- Allow BC++ projects to use the char * operator
		// while we convert the rest across
#endif // _MSC_VER
	char *rawAccess();
			// Provide access to the interal buffer.
			// This pointer is only guaranteed until the next time
			// a non-const operation is performed on the string.

	// Streaming
//	friend ostream &operator<<(ostream &out, const cString &string) { string.output( out ); return out; }
			// Simply calls output() and discards the error.

//	friend istream &operator>>(istream &in, cString &string) { string.input( in ); return in; }
			// Simply calls input(), and discards the error.

private:

	void	_detach();
			// Private function to detach reference strings.

	struct _srep
	{
		// Pointer to data
	#ifdef _MT
		volatile char *string;
		volatile int number;
		volatile size_t bufferLength;
		volatile mutable size_t cacheLength; // String length cache 0 if not cached
		CRITICAL_SECTION _sect;
	#else
		char 	*string;
		int     number;
		size_t 	bufferLength;
		mutable size_t  cacheLength; // String length cache 0 if not cached
	#endif //_MT

		_srep()
		{
			number = 1;
			bufferLength = 1;
			cacheLength = 0;
			// Make sure that we are always pointing to something.
			string = new char[1];
			string[0] = NULL;
		#ifdef _MT
			::InitializeCriticalSection(&_sect); 
		#endif //_MT
		}

		_srep( size_t length )
		{
			assert( length > 0 );
			number = 1;
			bufferLength = length;
			cacheLength = 0;
			string = new char[ bufferLength ];
			string[0] = NULL;
		#ifdef _MT
			::InitializeCriticalSection(&_sect); 
		#endif //_MT
		}

		_srep( const char *newString )
		{
			number = 1;
			bufferLength = strlen( newString ) + 1;
			cacheLength = 0;
			string = new char[ bufferLength ];
			strcpy( (char*)string, newString );
		#ifdef _MT
			::InitializeCriticalSection(&_sect); 
		#endif //_MT
		}

		~_srep()
		{
			delete [] (char*)string;
			string = NULL;
		#ifdef _MT
			assert( _sect.LockCount == -1 );
			::DeleteCriticalSection(&_sect); 
		#endif //_MT
		}
	};

	_srep *_string;
};


//
// Global Functions
//

inline char *strcat(cString &dst, const char *src) { dst.append( src ); return ( dst.rawAccess() ); }
inline char *strcpy(cString &dst, const char *src) { dst = src; return dst.rawAccess(); }
char *strncat(cString &dst, const char *src, size_t len);
char *strncpy(cString &dst, const char *src, size_t len);
		// Identical to the normal C functions of the same names, with the
		// exception that the dst string will be automatically sized to be big
		// enough. (And that cStrings can never be unterminated, and the usual
		// versions of strncat and strncpy can produce unterminated strings).

int sprintf(cString &dst, const char *fmt, ...);
int vsprintf(cString &dst, const char *fmt, va_list arglist);
		// Identical to the normal sprintf, except that the dst string will
		// be automatically be sized to be big enough for the result.

//
// Functions to stop borland shitting itself
//
inline char *		strchr(cString & s, int c) { return::strchr(s.rawAccess(), c); }
inline const char *	strchr(const cString & s, int c) { return::strchr((const char *)s, c); }
inline char *		strrchr(cString & s, int c) { return::strrchr(s.rawAccess(), c); }
inline const char *	strrchr(const cString & s, int c) { return::strrchr((const char *)s, c); }
inline char *		strpbrk(cString & s1, const char *s2) { return::strpbrk(s1.rawAccess(), s2); }
inline const char *	strpbrk(const cString & s1, const char *s2) { return::strpbrk((const char *)s1, s2); }
inline char *		strstr(cString & s1, const char *s2) { return::strstr(s1.rawAccess(), s2); }
inline const char *	strstr(const cString & s1, const char *s2) { return::strstr((const char *)s1, s2); }

#endif // FDT_STRING_HPP


