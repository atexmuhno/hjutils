// fdt/bstring.cpp - BSTR string wrapper
// Copyright 1999 Cybergraphic Systems.  All rights reserved

//#ifdef USE_LIBRARY_HPP
//#include "library.hpp"
//#else
//#include <windows.h>
//#endif

#include "stdafx.h"
#include <oleauto.h>
#include "bstring.hpp"


// default constructor
cSimpleBString::cSimpleBString()
   	:	_bstr(NULL)
{
};

// constructing with BSTR will not free the BSTR on destruction
// since cSimpleBString does not own nor free the BSTR.
cSimpleBString::cSimpleBString( BSTR bstr )
   	:	_bstr( bstr )
{
};

// copy constructor causes copy of BSTR without new allocation
// since cSimpleBString does not own nor free the BSTR.
cSimpleBString::cSimpleBString( const cSimpleBString &bstr )
{
    _bstr = bstr._getBSTR();
};

// construction with 'const char *' causes allocation of new BSTR.
// this is so cSimpleBString can be used to allocate a new BSTR
// and return this through a return argument without freeing the BSTR
// on cSimpleBString desctruction.
cSimpleBString::cSimpleBString( const char *str )
{
	_assignNewBSTR(str);
};

// construction with cString causes allocation of new BSTR.
// this is so cSimpleBString can be used to allocate a new BSTR
// and return this through a return argument without freeing the BSTR
// on cSimpleBString desctruction.
cSimpleBString::cSimpleBString( const cString &str )
{
	_assignNewBSTR((const char*) str);
};

// destructor of cSimpleBString does not free the BSTR.
// derived cBString destructor will free the BSTR.
cSimpleBString::~cSimpleBString()
{
};

// returns the BSTR.
cSimpleBString::operator BSTR() const
{
   	return _getBSTR();
};

// converts the BSTR into a cString.
cSimpleBString::operator cString() const
{
	int	length = WideCharToMultiByte( CP_ACP, 0, _bstr, -1, NULL, 0, NULL, NULL );
	assert(length>=0);
	cString	string( (size_t) (length?length:1) );
	WideCharToMultiByte( CP_ACP, 0, _bstr, -1, string.rawAccess(), length, NULL, NULL );
	return string;
};


//
// Return a pointer to the internal BSTR.
//

BSTR *
cSimpleBString::operator &()
{
	// Sanity -- make sure we have a NULL BSTR
	assert( _bstr == NULL );

	// Return the address of our BSTR
	return &_bstr;
}


// assignment causes copy of BSTR without new allocation
// since cSimpleBString does not own nor free the BSTR.
cSimpleBString&
cSimpleBString::operator= ( const cSimpleBString &bstr )
{
    _bstr = bstr._getBSTR();
	return *this;
}

// case sensitive comparison
bool 
cSimpleBString::operator== ( const cSimpleBString &bstr )
{
	return cString( *this ) == cString( bstr );
}

// case sensitive comparison
bool 
cSimpleBString::operator!= ( const cSimpleBString &bstr )
{
	return ! operator==( bstr );
}

// return the contained BSTR.
BSTR
cSimpleBString::_getBSTR(void) const
{
  	return _bstr;
};

// free the contained BSTR.
void
cSimpleBString::_freeBSTR(void)
{
	if(_bstr!=NULL)
	{
		SysFreeString(_bstr);
		_bstr = NULL;
	}
}

// converts the string into a wide-char string, allocates
// a new BSTR and assigns this to the contained BSTR member.
void
cSimpleBString::_assignNewBSTR(const char *str)
{
	int		length = MultiByteToWideChar( CP_ACP, 0, str, -1, NULL, 0 );
	OLECHAR	*temp = new OLECHAR[ length ];
	MultiByteToWideChar( CP_ACP, 0, str, -1, temp, length );
	_bstr = SysAllocString( temp );
	delete[] temp;
}

// allocates a new BSTR and assigns this to the contained BSTR member.
void
cSimpleBString::_assignNewBSTR(BSTR bstr)
{
	_bstr = SysAllocString( bstr );
}

// default constructor
cBString::cBString()
	:	cSimpleBString()
{
};

// cBString will own and free the contained BSTR.
cBString::cBString( BSTR bstr )
	:	cSimpleBString( bstr )
{
};

// copy constructor causes free of contained BSTR, allocation of
// new BSTR and assiging this to BSTR member. cBString destructor
// will free the contained BSTR.
cBString::cBString( const cBString &bstr )
{
	_freeBSTR();
	_assignNewBSTR( (BSTR) bstr );
};

// construction with 'const char *' causes allocation of new BSTR.
// cBString will free the contained BSTR on destruction.
cBString::cBString( const char *str )
   	:	cSimpleBString( str )
{
};

// construction with cString causes allocation of new BSTR.
// cBString will free the contained BSTR on destruction.
cBString::cBString( const cString &str )
   	:	cSimpleBString( str )
{
};

// destructor will free the contained BSTR because cBString
// is considered to own the BSTR and is responsible for freeing the
// BSTR. The cSimpleBString desctructor does not free the contained BSTR
// so that cSimpleBString can be used to allocate a new BSTR but transfer
// ownership of the BSTR and the responsibility of freeing the BSTR.
cBString::~cBString()
{
   	_freeBSTR();
}

// assignment operator causes free of contained BSTR, allocation of
// new BSTR and assiging this to BSTR member. cBString destructor
// will free the contained BSTR.
cBString&
cBString::operator= ( const cBString &bstr )
{
	if ( ((BSTR) *this) != ((BSTR) bstr) )
	{
		_freeBSTR();
		_assignNewBSTR( (BSTR) bstr );
	}
	return *this;
}

