// variant.hpp -- wrapper for VARIANT data type
// Copyright 1999 Cybergraphic Systems.  All rights reserved. 
//


#ifndef VARIANT_HPP
#define VARIANT_HPP

class cError;
class cBString;
class cString;
class cDateTime;



#include "fdt/array.hpp"

// cVariant
/////////////

class cVariant : public tagVARIANT
{
	public:
		cVariant();
			// Constructor
		cVariant( const VARIANT &var );
			// constructor
		cVariant( const cVariant & var );
			// constructor
		cVariant( const cBString &bstr );
			// constructor
		cVariant( LPCOLESTR str );
			// constructor
		cVariant( const char *str );
			// constructor
		cVariant( bool b );
			// constructor
		cVariant( int i );
			// constructor
		cVariant( unsigned int i );
			// constructor
		cVariant( char c );
			// constructor
		cVariant( unsigned char c );
			// constructor
		cVariant( short s );
			// constructor
		cVariant( unsigned short s );
			// constructor
		cVariant( long l, VARTYPE type = VT_I4 );
			// constructor
		cVariant( unsigned long l, VARTYPE type = VT_UI4 );
			// constructor
		cVariant( float f );
			// constructor
		cVariant( double d );
			// constructor
		cVariant( CY cy );
			// constructor
		cVariant( IDispatch* src );
			// constructor
		cVariant( IUnknown *src );
			// constructor
		cVariant( const cString &str );
			// constructor
		cVariant( const cDateTime &datetime );
			// constructor

		~cVariant();
			// Destructor

		// assignment operators
		cVariant& operator=( const cVariant &var );
			// assigment
		cVariant& operator=( const VARIANT &var );
			// assignment
		cVariant& operator=( const cBString &bstr );
			// assignment
		cVariant& operator=( LPCOLESTR str );
			// assignment
		cVariant& operator=( const char *str );
			// assignment
		cVariant& operator=( bool b );
			// assignment
		cVariant& operator=( int i );
			// assignment
		cVariant& operator=( unsigned int i );
			// assignment
		cVariant& operator=( char c );
			// assignment
		cVariant& operator=( unsigned char c );
			// assignment
		cVariant& operator=( short s );
			// assignment
		cVariant& operator=( unsigned short s );
			// assignment
		cVariant& operator=( long l );
			// assignment
		cVariant& operator=( unsigned long l );
			// assignment
		cVariant& operator=( float f );
			// assignment
		cVariant& operator=( double d );
			// assignment
		cVariant& operator=( CY cy );
			//assignment
		cVariant& operator=( IDispatch* src );
			// assignment
		cVariant& operator=( IUnknown *src );
			// assignment
		cVariant& operator=( const cString &str );
			// assigment

		// comparison operators
		bool operator==( const VARIANT &var ) const;
			// equality
		bool operator!=( const VARIANT &var ) const;
			// inequality
		bool operator<( const VARIANT &var ) const;
			// less than
		bool operator>( const VARIANT &var ) const;
			// greater than

		// Operations
		cError clear();
			// clear the variant structure
		cError Copy( const VARIANT *src );
			// copy the src variant to this
		cError attach( VARIANT *src );
			// attach the given variant
		cError detach( VARIANT *dest );
			// detach the variant, and place it in dest
		cError changeType( VARTYPE vtNew, const VARIANT *src = NULL );
			// change the type of the variant

		cError fromArray(const cArray<unsigned char> &val );
			// funkarama

		cError toArray( cArray<unsigned char> *retVal) const;
			// psychotropic

	private:
		cError internalClear();
		void internalCopy( const VARIANT *src );
};

#endif // VARIANT_HPP

