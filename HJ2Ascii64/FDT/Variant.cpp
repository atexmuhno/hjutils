// variant.cpp -- Variant wrapper class
// Copyright 1999 Cybergraphic Systems.  All rights reserved. 
//

#ifdef USE_LIBRARY_HPP
#include "library.hpp"
#else // USE_LIBRARY_HPP
//#error 
#include <assert.h>
#include <iostream.h>

#endif // USE_LIBRARY_HPP

#include "com/com.hpp"
#include "fdt/bstring.hpp"
#include "fdt/date.hpp"
#include "fdt/variant.hpp"

//cVariant
////////////

// Constructor
cVariant::cVariant()
{
	vt = VT_EMPTY;
}
// constructor
cVariant::cVariant( const VARIANT &var )
{
	vt = VT_EMPTY;
	internalCopy( &var );
}
// constructor
cVariant::cVariant( const cVariant & var )
{
	vt = VT_EMPTY;
	internalCopy( &var );
}
// constructor
cVariant::cVariant( const cBString &bstr )
{
	vt = VT_EMPTY;
	*this = bstr;
}
// constructor
cVariant::cVariant( LPCOLESTR str )
{
	vt = VT_EMPTY;
	*this = str;
}

// constructor
cVariant::cVariant( const char *str )
{
	vt = VT_EMPTY;
	*this = str;
}
// constructor
cVariant::cVariant( bool b )
{
	vt = VT_BOOL;
	boolVal = b ? VARIANT_TRUE : VARIANT_FALSE;
}
// constructor
cVariant::cVariant( int i )
{
	vt = VT_INT;
	intVal = i;
}
// constructor constructor
cVariant::cVariant( unsigned int i )
{
	vt = VT_UINT;
	uintVal = i;
}
cVariant::cVariant( char c )
{
	vt = VT_I1;
	cVal = c;
}
// constructor
cVariant::cVariant( unsigned char c )
{
	vt = VT_UI1;
	bVal = c;
}
// constructor
cVariant::cVariant( short s )
{
	vt = VT_I2;
	iVal = s;
}
// constructor
cVariant::cVariant( unsigned short s )
{
	vt = VT_UI2;
	uiVal = s;
}
// constructor
cVariant::cVariant( long l, VARTYPE type )
{
	assert( type == VT_I4 || type == VT_ERROR );
	vt = type;
	lVal = l;
}
// constructor constructor
cVariant::cVariant( unsigned long l, VARTYPE type )
{
	assert( type == VT_UI4 || type == VT_ERROR );
	vt = type;
	ulVal = l;
}
cVariant::cVariant( float f )
{
	vt = VT_R4;
	fltVal = f;
}
// constructor
cVariant::cVariant( double d )
{
	vt = VT_R8;
	dblVal = d;
}
// constructor
cVariant::cVariant( CY cy )
{
	vt = VT_CY;
	cyVal.Hi = cy.Hi;
	cyVal.Lo = cy.Lo;
}
// constructor
cVariant::cVariant( IDispatch* src )
{
	vt = VT_DISPATCH;
	pdispVal = src;
	// Need to AddRef as VariantClear will release
	if( pdispVal != NULL )
	{
		pdispVal->AddRef();
	}
}
// constructor
cVariant::cVariant( IUnknown *src )
{
	vt = VT_UNKNOWN;
	punkVal = src;
	// Need to AddRef as VariantClear will release
	if( punkVal != NULL )
	{
		punkVal->AddRef();
	}
}
// constructor
cVariant::cVariant( const cString &str )
{
	vt = VT_EMPTY;
	*this = str;
}
// constructor
cVariant::cVariant( const cDateTime &datetime )
{
	vt = VT_DATE;
	date = (DATE)datetime;
}

// Destructor
cVariant::~cVariant()
{
	clear();
}

		// assignment operators
// assigment
cVariant&
cVariant::operator=( const cVariant &var )
{
	internalCopy( &var );
	return *this;
}
// assignment
cVariant&
cVariant::operator=( const VARIANT &var )
{
	internalCopy( &var );
	return *this;
}
// assignment
cVariant&
cVariant::operator=( const cBString &bstr )
{
	internalClear();
	vt = VT_BSTR;
	bstrVal = ::SysAllocString( bstr );
	if( bstrVal == NULL && bstr != NULL )
	{
		vt = VT_ERROR;
		scode = E_OUTOFMEMORY;
	}
	return *this;
}
// assignment
cVariant&
cVariant::operator=( LPCOLESTR str )
{
	internalClear();
	vt = VT_BSTR;
	bstrVal = ::SysAllocString( str );
	if( bstrVal == NULL && str != NULL )
	{
		vt = VT_ERROR;
		scode = E_OUTOFMEMORY;
	}
	return *this;
}

// assignment
cVariant&
cVariant::operator=( const char *str )
{
	internalClear();
	vt = VT_BSTR;
	cSimpleBString bstr(str);
	bstrVal = bstr;
	return *this;
}
// assignment
cVariant&
cVariant::operator=( bool b )
{
	if( vt != VT_BOOL )
	{
		internalClear();
		vt = VT_BOOL;
	}
	boolVal = b ? VARIANT_TRUE : VARIANT_FALSE;
	return *this;
}
// assignment
cVariant&
cVariant::operator=( int i )
{
	if( vt != VT_INT )
	{
		internalClear();
		vt = VT_INT;
	}
	intVal = i;
	return *this;
}
// assignment
cVariant&
cVariant::operator=( unsigned int i )
{
	if( vt != VT_UINT )
	{
		internalClear();
		vt = VT_UINT;
	}
	uintVal = i;
	return *this;

}

// assignment
cVariant&
cVariant::operator=( char c )
{
	if( vt != VT_I1 )
	{
		internalClear();
		vt = VT_I1;
	}
	cVal = c;
	return *this;
}
// assignment
cVariant&
cVariant::operator=( unsigned char c )
{
	if( vt != VT_UI1 )
	{
		internalClear();
		vt = VT_UI1;
	}
	bVal = c;
	return *this;

}

// assignment
cVariant&
cVariant::operator=( short s )
{
	if( vt != VT_I2 )
	{
		internalClear();
		vt = VT_I2;
	}
	iVal = s;
	return *this;
}
// assignment
cVariant&
cVariant::operator=( unsigned short s )
{
	if( vt != VT_UI2 )
	{
		internalClear();
		vt = VT_UI2;
	}
	uiVal = s;
	return *this;

}

// assignment
cVariant&
cVariant::operator=( long l )
{
	if( vt != VT_I4 )
	{
		internalClear();
		vt = VT_I4;
	}
	lVal = l;
	return *this;
}
// assignment assignment
cVariant&
cVariant::operator=( unsigned long l )
{
	if( vt != VT_UI4 )
	{
		internalClear();
		vt = VT_UI4;
	}
	ulVal = l;
	return *this;
}
cVariant&
cVariant::operator=( float f )
{
	if( vt != VT_R4 )
	{
		internalClear();
		vt = VT_R4;
	}
	fltVal = f;
	return *this;
}
// assignment
cVariant&
cVariant::operator=( double d )
{
	if( vt != VT_R8 )
	{
		internalClear();
		vt = VT_R8;
	}
	dblVal = d;
	return *this;
}
// assignment
cVariant&
cVariant::operator=( CY cy )
{
	if( vt != VT_CY)
	{
		internalClear();
		vt = VT_CY;
	}
	cyVal.Hi = cy.Hi;
	cyVal.Lo = cy.Lo;
	return *this;
}
// assignment
cVariant&
cVariant::operator=( IDispatch* src )
{
	internalClear();
	vt = VT_DISPATCH;
	pdispVal = src;
	// Need to AddRef as VariantClear will Release
	if( pdispVal != NULL )
	{
		pdispVal->AddRef();
	}
	return *this;
}
// assignment
cVariant&
cVariant::operator=( IUnknown *src )
{
	internalClear();
	vt = VT_UNKNOWN;
	punkVal = src;
	// Need to AddRef as VariantClear will Release
	if( punkVal != NULL )
	{
		punkVal->AddRef();
	}
	return *this;
}
// assigment
cVariant&
cVariant::operator=( const cString &str )
{
	internalClear();
	vt = VT_BSTR;
	cSimpleBString bstr(str);
	bstrVal = bstr;
	return *this;
}


// comparison operators
// equality
bool
cVariant::operator==( const VARIANT &var ) const
{
	if( this == &var )
	{
		return true;
	}

	// Variants not equal if types don't match
	if( vt != var.vt )
	{
		return false;
	}

	switch( vt )
	{
		case VT_EMPTY:
		case VT_NULL:
			return true;
		case VT_BOOL:
			return boolVal == var.boolVal;
		case VT_UI1:
			return bVal == var.bVal;
		case VT_I2:
			return iVal == var.iVal;
		case VT_I4:
			return lVal == var.lVal;
		case VT_R4:
			return fltVal == var.fltVal;
		case VT_R8:
			return dblVal == var.dblVal;
		case VT_BSTR:
			return (::SysStringByteLen(bstrVal) == ::SysStringByteLen(var.bstrVal))
				&& (::memcmp(bstrVal,var.bstrVal,::SysStringByteLen(bstrVal)) == 0);
		case VT_ERROR:
			return scode == var.scode;
		case VT_DISPATCH:
				return pdispVal == var.pdispVal;
		case VT_UNKNOWN:
			return punkVal == var.punkVal;
		default:
			assert(false);
	}
	return false;
}
// inequality
bool
cVariant::operator!=( const VARIANT &var ) const
{
	return !(*this == var);
}
// less than
bool
cVariant::operator<( const VARIANT &var ) const
{
	return VarCmp(const_cast<VARIANT*>((const VARIANT *)(this)), const_cast<VARIANT *>(&var), LOCALE_USER_DEFAULT, 0)==VARCMP_LT;
}
// greater than
bool
cVariant::operator>( const VARIANT &var ) const
{
	return VarCmp(const_cast<VARIANT*>((const VARIANT *)(this)), const_cast<VARIANT *>(&var), LOCALE_USER_DEFAULT, 0)==VARCMP_GT;
}

// Operations
// clear the variant structure
cError
cVariant::clear()
{
	HRESULT hr = ::VariantClear(this);
	return cCOM::error(hr);
}
// copy the src variant to this
cError
cVariant::Copy( const VARIANT *src )
{
	HRESULT hr = ::VariantCopy(this,const_cast<VARIANT*>(src));
	return cCOM::error(hr);
}
// attach the given variant
cError
cVariant::attach( VARIANT *src )
{
	// clear out the variant
	HRESULT hr = clear();
	if( !FAILED(hr) )
	{
		// Copy the contents and give control to the cVariant
		memcpy(this,src,sizeof(VARIANT));
		src->vt = VT_EMPTY;
		hr = S_OK;
	}
	return hr;
}
// detach the variant, and place it in dest
cError
cVariant::detach( VARIANT *dest )
{
	HRESULT hr = ::VariantClear( dest );
	if( !FAILED(hr) )
	{
		// Copy the contents and remove control from cVariant
		memcpy( dest, this, sizeof(VARIANT) );
		vt = VT_EMPTY;
		hr = S_OK;
	}
	return cCOM::error(hr);
}
// change the type of the variant
cError
cVariant::changeType( VARTYPE newType, const VARIANT *src )
{
	VARIANT *var = const_cast<VARIANT*>(src);
	if( var == NULL )
	{
		var = this;
	}
	// Do nothing if doing in place convert and vts not different
	HRESULT hr = ::VariantChangeType(this,var,0,newType);
	return cCOM::error(hr);
}

// TODO: Comment your functions! XXX
cError
cVariant::internalClear()
{
	HRESULT hr = clear();
	assert(SUCCEEDED(hr));
	if( FAILED(hr) )
	{
		vt = VT_ERROR;
		scode = hr;
	}
	return cCOM::error(hr);
}
// TODO: Comment your functions! XXX
void
cVariant::internalCopy( const VARIANT *src )
{
	HRESULT hr = Copy( src );
	if( FAILED(hr) )
	{
		vt = VT_ERROR;
		scode = hr;
	}
}


//Converts a cArray<unsigned char> to a cVariant
cError 
cVariant::fromArray(const cArray<unsigned char> &val )
{
	cError err;
	
	SAFEARRAY * psa;
	HRESULT hr;
	SAFEARRAYBOUND rgsabound[1];
	rgsabound[0].lLbound = 0;
	rgsabound[0].cElements = val.size();
	psa = SafeArrayCreate(VT_UI1, 1, rgsabound);
	if(psa == NULL)
	{
		return cError(es_error,"Could not create Safearray.");
	}
	
	BYTE * pData;
	hr = SafeArrayAccessData(psa,(void **) &pData);
	if (FAILED(hr)) 
	{
		return cError(hr);
	}

	val.export(pData,val.size());
	SafeArrayUnaccessData(psa);

	// Initialize Variant
	clear();
	vt = VT_ARRAY | VT_UI1;
	parray = psa;
	
	return err;
}


cError 
cVariant::toArray( cArray<unsigned char> *retVal) const
{		
	cError err;
		
	// Make sure we have the correct variant type
	if (  vt != (VT_ARRAY | VT_UI1) )
	{
		return cError(es_error, "Invalid variant type for conversion");
	}

	// Find the length and data in the variant
	size_t	length =  parray->rgsabound[0].cElements -  parray->rgsabound[0].lLbound;
	
	BYTE * pData;
	
	HRESULT hr = SafeArrayAccessData( parray,(void **) &pData);
	if (FAILED(hr)) 
	{
		return cError(hr);
	}

	retVal->import((BYTE *)pData, length);
	SafeArrayUnaccessData( parray);
	return err;
}


