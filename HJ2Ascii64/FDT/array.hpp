/** \file  fdt\array.hpp
 * Auto-expanding array.
 * Copyright 1999 Cybergraphic Systems.  All rights reserved.
 */

#ifndef FDT_ARRAY_HPP
#define FDT_ARRAY_HPP


// cArray
/////////////

/** Auto-expanding array class. This is a templated array class that expands as
 * required.
 */

template <typename T>
class cArray
{
	public:
		/** Function pointer to an element initialiser.
		 * The paramaters are this way in order that the initialiser can access
		 * previous elements of the array.
		 * \param array Pointer to the \b start of the \b array memory.
		 * \param first Element to start initialisation at.
		 * \param last  Element to end initilalisation at.
		 * \sa ZeroElements
		 */
		typedef void (*InitElement)(T *array, size_t first,size_t last);

		/** Constructor.
		 * \param increment The incremement in size of the array.
		 * \param init An initialiser function (defaults to none).
		 * \sa ZeroElements
		 */
		cArray(int increment=5,InitElement init = 0 )
			: _array(0),_elements(0), _increment(increment),_init(init)
			{}

		/** Constructor.
		 * If copy is true, construct the array by copying count elements from
		 * the passed in array, otherwise take ownership of the array. When
		 * taking ownership the array must have been created with the same
		 * mecahnism as _allocate().
		 */
		cArray( T * source, size_t count, bool copy = true, int increment = 5, InitElement init = 0 ) :
			_array(0), _elements(0), _increment(increment), _init(init)
			{
				if ( copy )
				{
					_resize( count );
					for ( int i = 0; i < count; i++ )
					{
						_array[i] = source[i];
					}
				}
				else
				{
					_array = source;
					_elements = count;
				}
			}

		/// Destructor
		virtual ~cArray()
		{
			_clear();
		}

		/// Get the size of the array.
		size_t size() const
		{
			return _elements;
		}

		/** Set the size of the array. This grows or shrinks the array to
		 * exactly the specified size.
		 */
		void size(size_t s)
		{
			if(size()!=s)
			{
				_resize(s);
			}
		}


		/** Set the minimum size of the array. This grows or shrinks the array
		 * so that it is the smallest multiple of the \e increment that will
		 * accomodate \a s elements.
		 * \param s Number of elements to be accomodated.
		 */
		void sizeTo(size_t s)
		{
			size( ((s/_increment)+1) * _increment );
		}

		/** Element access. Access element \a i of the array.  This will expand
		 * the array if needed.
		 */
		T &operator[](int i)
		{
			//assert(i>=0);
			if(i>=_elements)
			{
				int elements=_elements+_increment;
				if(i>=elements)
				{
					elements= ((int)((elements+i)/_increment))*_increment;
				}
				_resize(elements);
				if(i>=_elements)
				{
					//assert(0);
					//return _array[0];
					i=0;
				}
			}
			return _array[i];
		}

		/** Constant element access.
		 * The element must already exist.
		 */
		T operator[](int i) const
		{
			if ( i >= _elements || i < 0 )
			{
				assert(!"Array out of bounds");
				i=0;
			}
			return _array[i];
		}

		/** Copy the current array into the passed in buffer.
		 * Returns the number of elements copied.
		 */
		int exportx( T *destination, size_t bufferSize ) const
		{
			if ( bufferSize > _elements )
			{
				bufferSize = _elements;
			}
			for ( int i = 0; i < bufferSize; i++ )
			{
				destination[i] = _array[i];
			}
			return bufferSize;
		}

		/** Copy count elements from the input buffer.
		 * This overwrite the current contents of the array. The number of elements
		 * copied is returned.
		 */
		int import( const T  * source, size_t count )
		{
			_resize(count);
			if (count > _elements)
			{
				assert(0);
				return 0;
			}
			for ( int i = 0; i < count; i++ )
			{
				_array[i] = source[i];
			}
			return count;
		}

		/** Release the array, passing back the pointer to it.
		 * This array must be deleted using the same mecahnism as _clear();
		 */
		T *release()
		{
			T	*retval = _array;
			_array = NULL;
			_elements =0;
			return retval;
		}

	protected:

		/** Resize the array to \a elements.
		 */
		void _resize(int elements)
		{
			// Work out how many old elements to copy
			int oldelements=(elements<_elements?elements:_elements);
			T *array= _allocate(elements);
			if(array!=NULL)
			{
				// Initialise the new area first
				if(_init!=0 && elements > _elements )
				{
					(*_init)(array, oldelements, elements-1);
				}
				// Copy old values across
				for(int i=0;i<oldelements;++i)
				{
					array[i]=_array[i];
				}
				// delete the old array
				_clear();

				_array=array;
				_elements=elements;
			}
		}

		/** Allocate memory. 
		  */
		virtual T * _allocate(int elements)
		{
			return new T[elements];
		}

		/** Delete memory.
		  */
		virtual void _clear()
		{
			delete [] _array;
			_array=NULL;
		}

		/// Pointer to the array.
		T *_array;

	private:
		cArray( const cArray & orig) {}
			///< Disallow copy constructor

		void operator = (const cArray & rhs) {}
			///< Disallow assignment

		int _elements;
		int _increment;
		InitElement _init;
};


template <typename T>
class cArrayMalloc : public cArray<T>
{
public:

	cArrayMalloc(int increment=5,InitElement init = 0 ) : cArray<T>(increment,init) {}
	cArrayMalloc( T * source, size_t count, bool copy = true, int increment = 5, InitElement init = 0 ) :
		cArray<T>( copy ? NULL : source, copy ? 0 : count, false, increment, init)
		{
			// We must handle copying here to ensure the correct allocator get
			// used (hence the conditional operators in the base class
			// constructor call).
			if ( copy )
			{
				import( source, count );
			}
		}

	virtual ~cArrayMalloc() { _clear(); }
			/**< Destructor.
			 * We must override the destructor and clear here, or else the base
			 * class will try to free memory with delete[].
			 */

protected:

	T * _allocate(int elements)
	{
		return (T *) malloc(sizeof(T) * elements);
	}

	void _clear()
	{
		if ( _array != NULL )
		{
			free(_array);
			_array = NULL;
		}
	}
};

/** A templated function for initialising elements in an array.
 * The function matches the prototype for cArray::InitElement and is for use
 * with integral types and pointers only.
 * \warning Uses memset, so don't use it for classes.
 */
template <typename T>
void ZeroElements(T *array,size_t first, size_t last)
{
	memset(array+first,(1+last-first)*sizeof(T),0);
}


#endif // FDT_ARRAY_HPP


