// HJ/Hj2ASCII/CharacterTranslation.hpp
/**\file
 *\ingroup hj
 * Declaration of cCharacterTranslation
 * Copyright 2001 Geac Publishing Systems. All rights reserved
 */

#ifndef __CHARACTERTRANSLATION_H_
#define __CHARACTERTRANSLATION_H_

#include "resource.h"       // main symbols
#include "fdt/string.hpp"

/** The cCharacterTranslation class is used to specify and perform simple character to
 *   string translation operations
 */

/////////////////////////////////////////////////////////////////////////////
// cCharacterTranslation
class ATL_NO_VTABLE cCharacterTranslation : 
	public CComObjectRootEx<CComSingleThreadModel>,
//	public CComCoClass<cCharacterTranslation, &CLSID_CharacterTranslation>,
	public IDispatchImpl<ICharacterTranslation, &IID_ICharacterTranslation, &LIBID_HJ2ASCIILib>
{
public:
	cCharacterTranslation()
	{
	}

//DECLARE_REGISTRY_RESOURCEID(IDR_CHARACTERTRANSLATION)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cCharacterTranslation)
	COM_INTERFACE_ENTRY(ICharacterTranslation)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// ICharacterTranslation
public:
	STDMETHOD(ConvertString)(/*[in]*/ BSTR inString,/*[out,retval]*/BSTR *outString);
		/**< Convert a tring of characters
		 *\param inString		- The string to be converted
		 *\param outSTring		- The corresponding translation
		 */
	STDMETHOD(get_Translation)(/*[in]*/long ival, /*[out]*/ BSTR* Translation );
		/**< Get the translation string corresponding to a specific character
		 *\param ival			- The character code
		 *\param Translation	- The corresponding translation
		 */
	STDMETHOD(put_Translation)(/*[in]*/long ival, /*[in]*/  BSTR Translation );
		/**< Store the translation string corresponding to a specific character
		 *\param ival			- The character code
		 *\param Translation	- The corresponding translation
		 */
	STDMETHOD(AddPrintables)();
		/**< Set default translation for all printable characters
		 */
private:
	cString m_map[256];
};

#endif //__CHARACTERTRANSLATION_H_
