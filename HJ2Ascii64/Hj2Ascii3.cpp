// Hj2Ascii3.cpp : Implementation of cHj2Ascii3
#include "stdafx.h"

#ifdef USE_LIBRARY_HPP
#include "library.hpp"
#else
#include <windows.h>
#endif

#include "HJ2Ascii.h"
#include "Hj2Ascii3.hpp"
#include "fdt/bstring.hpp"
#include "commandlist.hpp"
#include "taglist.hpp"
#include "charactertranslation.hpp"
#include "source.hpp"

/////////////////////////////////////////////////////////////////////////////
// cHj2Ascii3
cHj2Ascii3::cHj2Ascii3()
:
	m_DeferEOLOnHyphen(false),
	m_IgnoreNonSource(false),
	m_IgnoreErrorMessages(true),
	m_SubstituteLigatures(false),
	m_CharacterTranslationMode(cmAsIs),
	m_skip(false),
	m_Note(false),
	m_NonSourceCount(0)

{
	m_EOLString = cString("\n");
};

STDMETHODIMP cHj2Ascii3::FinalConstruct()
{
	HRESULT hr;

	// Create an instance of the command list
	hr = CComCreator<CComObject<cCommandList> > ::CreateInstance( NULL, IID_ICommandList, reinterpret_cast<void**>(&m_Commands) );
	if (FAILED(hr)) return hr;

	// Create an instance of the tag list
	hr = CComCreator<CComObject<cTagList> > ::CreateInstance( NULL, IID_ITagList, reinterpret_cast<void**>(&m_Tags) );
	if (FAILED(hr)) return hr;

	// Create an instance of the character map
	hr = CComCreator<CComObject<cCharacterTranslation> > ::CreateInstance( NULL, IID_ICharacterTranslation, reinterpret_cast<void**>(&m_CharMap) );
	if (FAILED(hr)) return hr;

	// Create an instance of the source stream
	hr = CComCreator<CComObject<cSource> > ::CreateInstance( NULL, IID_ISource, reinterpret_cast<void**>(&m_Source) );

	return hr;

}

STDMETHODIMP cHj2Ascii3::get_EOLString(/*[out, retval]*/ BSTR* pVal)
{
	if (pVal == NULL) return E_POINTER;

	*pVal = cSimpleBString( m_EOLString );
	return S_OK;
}

STDMETHODIMP cHj2Ascii3::put_EOLString(/*[in]*/ BSTR newVal)
{
	m_EOLString = cSimpleBString( newVal );
	return	S_OK;
}
STDMETHODIMP cHj2Ascii3::get_DeferEOLOnHyphen(BOOL* pVal)
{
	if (pVal == NULL) return E_POINTER;

	*pVal = m_DeferEOLOnHyphen;
	return	S_OK;
}

STDMETHODIMP cHj2Ascii3::put_DeferEOLOnHyphen(BOOL newVal)
{
	m_DeferEOLOnHyphen = newVal != 0;
	return	S_OK;
}

STDMETHODIMP cHj2Ascii3::get_CharacterTranslationMode(long* pVal)
{
	if (pVal == NULL) return E_POINTER;

	*pVal = m_CharacterTranslationMode;
	return	S_OK;
}

STDMETHODIMP cHj2Ascii3::put_CharacterTranslationMode(long newVal)
{
	if ((newVal < 0) || (newVal > cmTranslationTable)) return E_FAIL;
	m_CharacterTranslationMode = cCharacterMode( newVal );
	return	S_OK;
}

STDMETHODIMP cHj2Ascii3::get_IgnoreErrorMessages(BOOL* pVal)
{
	if (pVal == NULL) return E_POINTER;

	*pVal = m_IgnoreErrorMessages;
	return	S_OK;
}

STDMETHODIMP cHj2Ascii3::put_IgnoreErrorMessages(BOOL newVal)
{
	m_IgnoreErrorMessages = newVal != 0;
	return	S_OK;
}

STDMETHODIMP cHj2Ascii3::get_IgnoreNonSource(BOOL* pVal)
{
	if (pVal == NULL) return E_POINTER;

	*pVal = m_IgnoreNonSource;
	return S_OK;
}

STDMETHODIMP cHj2Ascii3::put_IgnoreNonSource(BOOL newVal)
{
	m_IgnoreNonSource = newVal != 0;
	return S_OK;
}

STDMETHODIMP cHj2Ascii3::get_SubstituteLigatures(BOOL* pVal)
{
	if (pVal == NULL) return E_POINTER;

	*pVal = m_SubstituteLigatures;
	return S_OK;

}

STDMETHODIMP cHj2Ascii3::put_SubstituteLigatures(BOOL newVal)
{
	m_SubstituteLigatures = newVal != 0;
	return S_OK;
}

STDMETHODIMP cHj2Ascii3::get_TranslationStrings(ICommandList** pVal)
{
	if (pVal == NULL) return E_POINTER;

	m_Commands.CopyTo( pVal );;
	return S_OK;
}

STDMETHODIMP cHj2Ascii3::get_TagStrings(ITagList** pVal)
{
	if (pVal == NULL) return E_POINTER;

	m_Tags.CopyTo(pVal);
	return S_OK;
}

STDMETHODIMP cHj2Ascii3::putSource(VARIANT newVal, BOOL bPacked)
{
	// Reset H&J Parser conditions
	m_held	= false;
	m_Note	= false;
	m_NonSourceCount = 0;
	m_ligature = false;
	m_DeferEOL = false;
	m_TagsMode  = tmSkipToTag;
	m_PrevCommand = cString("");
	m_skip = false;

	return m_Source->put_Source( newVal, bPacked);
}

STDMETHODIMP cHj2Ascii3::get_CharacterStrings(ICharacterTranslation** pVal)
{
	if (pVal == NULL) return E_POINTER;

	m_CharMap.CopyTo( pVal );
	return S_OK;
}

STDMETHODIMP cHj2Ascii3::FullConvert(BSTR* Output)
{
	if (Output == NULL) return E_POINTER;

//	m_Commands->Add( cSimpleBString("*"), cSimpleBString("*") );

	*Output = cSimpleBString( cString("") );
	m_TagsMode = tmIgnoreTags;

	// If no input to process, finish
	if (m_Source->endOfFile() == S_OK)
		return S_FALSE;

	// Estimate size of output = size of input
	long	length;
	m_Source->get_length( &length );

	// Set output to size of input
	m_Output = *new cString( length );

	// Enable output
	m_skip = false;

	// Convert H&J
	parsehj();

	// Return the result
	*Output = cSimpleBString( m_Output );

	return S_OK;
}

STDMETHODIMP cHj2Ascii3::PartialConvert(BSTR* Output)
{
	if (Output == NULL) return E_POINTER;
	*Output = cSimpleBString( cString("") );

	// If no input to process, finish
	if (m_Source->endOfFile() == S_OK)
		return S_FALSE;

	// Estimate size of output = size of input
	long	length;
	m_Source->get_length( &length );

	// Set output to size of input
	m_Output = *new cString( length );

	if (m_TagsMode == tmIgnoreTags) m_TagsMode = tmSkipToTag;

	// Convert H&J
	parsehj();

	// Return the result
	*Output = cSimpleBString( m_Output );

	// return S_FALSE, if failed to find the correct tag
	if ((m_TagsMode == tmSkipToTag) && (m_Source->endOfFile() == S_OK))
		return S_FALSE;
	else return S_OK;
}
