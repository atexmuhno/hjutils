

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Tue Feb 09 14:54:40 2016
 */
/* Compiler settings for HJ2Ascii.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __HJ2Ascii_h__
#define __HJ2Ascii_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IHJ2Ascii_FWD_DEFINED__
#define __IHJ2Ascii_FWD_DEFINED__
typedef interface IHJ2Ascii IHJ2Ascii;
#endif 	/* __IHJ2Ascii_FWD_DEFINED__ */


#ifndef __IHJ2Ascii2_FWD_DEFINED__
#define __IHJ2Ascii2_FWD_DEFINED__
typedef interface IHJ2Ascii2 IHJ2Ascii2;
#endif 	/* __IHJ2Ascii2_FWD_DEFINED__ */


#ifndef __IHj2Ascii3_FWD_DEFINED__
#define __IHj2Ascii3_FWD_DEFINED__
typedef interface IHj2Ascii3 IHj2Ascii3;
#endif 	/* __IHj2Ascii3_FWD_DEFINED__ */


#ifndef __ICommandList_FWD_DEFINED__
#define __ICommandList_FWD_DEFINED__
typedef interface ICommandList ICommandList;
#endif 	/* __ICommandList_FWD_DEFINED__ */


#ifndef __ITagList_FWD_DEFINED__
#define __ITagList_FWD_DEFINED__
typedef interface ITagList ITagList;
#endif 	/* __ITagList_FWD_DEFINED__ */


#ifndef __ICharacterTranslation_FWD_DEFINED__
#define __ICharacterTranslation_FWD_DEFINED__
typedef interface ICharacterTranslation ICharacterTranslation;
#endif 	/* __ICharacterTranslation_FWD_DEFINED__ */


#ifndef __ISource_FWD_DEFINED__
#define __ISource_FWD_DEFINED__
typedef interface ISource ISource;
#endif 	/* __ISource_FWD_DEFINED__ */


#ifndef __HJ2Ascii_FWD_DEFINED__
#define __HJ2Ascii_FWD_DEFINED__

#ifdef __cplusplus
typedef class HJ2Ascii HJ2Ascii;
#else
typedef struct HJ2Ascii HJ2Ascii;
#endif /* __cplusplus */

#endif 	/* __HJ2Ascii_FWD_DEFINED__ */


#ifndef __Hj2Ascii3_FWD_DEFINED__
#define __Hj2Ascii3_FWD_DEFINED__

#ifdef __cplusplus
typedef class Hj2Ascii3 Hj2Ascii3;
#else
typedef struct Hj2Ascii3 Hj2Ascii3;
#endif /* __cplusplus */

#endif 	/* __Hj2Ascii3_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_HJ2Ascii_0000_0000 */
/* [local] */ 










extern RPC_IF_HANDLE __MIDL_itf_HJ2Ascii_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_HJ2Ascii_0000_0000_v0_0_s_ifspec;

#ifndef __IHJ2Ascii_INTERFACE_DEFINED__
#define __IHJ2Ascii_INTERFACE_DEFINED__

/* interface IHJ2Ascii */
/* [unique][helpstring][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IHJ2Ascii;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("32B32010-C172-11D2-8BF3-0080ADBA29A6")
    IHJ2Ascii : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Convert( 
            /* [in] */ VARIANT Source,
            /* [in] */ VARIANT_BOOL bPacked,
            /* [retval][out] */ BSTR *ptext) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHJ2AsciiVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IHJ2Ascii * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IHJ2Ascii * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IHJ2Ascii * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IHJ2Ascii * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IHJ2Ascii * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IHJ2Ascii * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IHJ2Ascii * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Convert )( 
            IHJ2Ascii * This,
            /* [in] */ VARIANT Source,
            /* [in] */ VARIANT_BOOL bPacked,
            /* [retval][out] */ BSTR *ptext);
        
        END_INTERFACE
    } IHJ2AsciiVtbl;

    interface IHJ2Ascii
    {
        CONST_VTBL struct IHJ2AsciiVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHJ2Ascii_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IHJ2Ascii_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IHJ2Ascii_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IHJ2Ascii_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IHJ2Ascii_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IHJ2Ascii_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IHJ2Ascii_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IHJ2Ascii_Convert(This,Source,bPacked,ptext)	\
    ( (This)->lpVtbl -> Convert(This,Source,bPacked,ptext) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IHJ2Ascii_INTERFACE_DEFINED__ */


#ifndef __IHJ2Ascii2_INTERFACE_DEFINED__
#define __IHJ2Ascii2_INTERFACE_DEFINED__

/* interface IHJ2Ascii2 */
/* [unique][helpstring][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IHJ2Ascii2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("227E2E50-3F2D-11d3-95C9-0080ADB80449")
    IHJ2Ascii2 : public IHJ2Ascii
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTranslationString( 
            /* [in] */ BSTR Command,
            /* [in] */ BSTR Translation) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTranslationString( 
            /* [in] */ BSTR Command,
            /* [retval][out] */ BSTR *translationString) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ClearTranslationStrings( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHJ2Ascii2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IHJ2Ascii2 * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IHJ2Ascii2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IHJ2Ascii2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IHJ2Ascii2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IHJ2Ascii2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IHJ2Ascii2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IHJ2Ascii2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Convert )( 
            IHJ2Ascii2 * This,
            /* [in] */ VARIANT Source,
            /* [in] */ VARIANT_BOOL bPacked,
            /* [retval][out] */ BSTR *ptext);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTranslationString )( 
            IHJ2Ascii2 * This,
            /* [in] */ BSTR Command,
            /* [in] */ BSTR Translation);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTranslationString )( 
            IHJ2Ascii2 * This,
            /* [in] */ BSTR Command,
            /* [retval][out] */ BSTR *translationString);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearTranslationStrings )( 
            IHJ2Ascii2 * This);
        
        END_INTERFACE
    } IHJ2Ascii2Vtbl;

    interface IHJ2Ascii2
    {
        CONST_VTBL struct IHJ2Ascii2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHJ2Ascii2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IHJ2Ascii2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IHJ2Ascii2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IHJ2Ascii2_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IHJ2Ascii2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IHJ2Ascii2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IHJ2Ascii2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IHJ2Ascii2_Convert(This,Source,bPacked,ptext)	\
    ( (This)->lpVtbl -> Convert(This,Source,bPacked,ptext) ) 


#define IHJ2Ascii2_SetTranslationString(This,Command,Translation)	\
    ( (This)->lpVtbl -> SetTranslationString(This,Command,Translation) ) 

#define IHJ2Ascii2_GetTranslationString(This,Command,translationString)	\
    ( (This)->lpVtbl -> GetTranslationString(This,Command,translationString) ) 

#define IHJ2Ascii2_ClearTranslationStrings(This)	\
    ( (This)->lpVtbl -> ClearTranslationStrings(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IHJ2Ascii2_INTERFACE_DEFINED__ */


#ifndef __IHj2Ascii3_INTERFACE_DEFINED__
#define __IHj2Ascii3_INTERFACE_DEFINED__

/* interface IHj2Ascii3 */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IHj2Ascii3;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("3E0244D9-46E0-4D61-B926-67E138212BBE")
    IHj2Ascii3 : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_EOLString( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_EOLString( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DeferEOLOnHyphen( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DeferEOLOnHyphen( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CharacterTranslationMode( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CharacterTranslationMode( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IgnoreErrorMessages( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_IgnoreErrorMessages( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IgnoreNonSource( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_IgnoreNonSource( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SubstituteLigatures( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SubstituteLigatures( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TranslationStrings( 
            /* [retval][out] */ ICommandList **pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TagStrings( 
            /* [retval][out] */ ITagList **pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CharacterStrings( 
            /* [retval][out] */ ICharacterTranslation **pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE putSource( 
            /* [in] */ VARIANT newVal,
            /* [in] */ BOOL bPacked) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FullConvert( 
            /* [out] */ BSTR *Output) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE PartialConvert( 
            /* [out] */ BSTR *Output) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHj2Ascii3Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IHj2Ascii3 * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IHj2Ascii3 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IHj2Ascii3 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IHj2Ascii3 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IHj2Ascii3 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IHj2Ascii3 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IHj2Ascii3 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EOLString )( 
            IHj2Ascii3 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_EOLString )( 
            IHj2Ascii3 * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DeferEOLOnHyphen )( 
            IHj2Ascii3 * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DeferEOLOnHyphen )( 
            IHj2Ascii3 * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CharacterTranslationMode )( 
            IHj2Ascii3 * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CharacterTranslationMode )( 
            IHj2Ascii3 * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IgnoreErrorMessages )( 
            IHj2Ascii3 * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_IgnoreErrorMessages )( 
            IHj2Ascii3 * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IgnoreNonSource )( 
            IHj2Ascii3 * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_IgnoreNonSource )( 
            IHj2Ascii3 * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SubstituteLigatures )( 
            IHj2Ascii3 * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SubstituteLigatures )( 
            IHj2Ascii3 * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TranslationStrings )( 
            IHj2Ascii3 * This,
            /* [retval][out] */ ICommandList **pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TagStrings )( 
            IHj2Ascii3 * This,
            /* [retval][out] */ ITagList **pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CharacterStrings )( 
            IHj2Ascii3 * This,
            /* [retval][out] */ ICharacterTranslation **pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *putSource )( 
            IHj2Ascii3 * This,
            /* [in] */ VARIANT newVal,
            /* [in] */ BOOL bPacked);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FullConvert )( 
            IHj2Ascii3 * This,
            /* [out] */ BSTR *Output);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *PartialConvert )( 
            IHj2Ascii3 * This,
            /* [out] */ BSTR *Output);
        
        END_INTERFACE
    } IHj2Ascii3Vtbl;

    interface IHj2Ascii3
    {
        CONST_VTBL struct IHj2Ascii3Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHj2Ascii3_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IHj2Ascii3_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IHj2Ascii3_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IHj2Ascii3_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IHj2Ascii3_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IHj2Ascii3_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IHj2Ascii3_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IHj2Ascii3_get_EOLString(This,pVal)	\
    ( (This)->lpVtbl -> get_EOLString(This,pVal) ) 

#define IHj2Ascii3_put_EOLString(This,newVal)	\
    ( (This)->lpVtbl -> put_EOLString(This,newVal) ) 

#define IHj2Ascii3_get_DeferEOLOnHyphen(This,pVal)	\
    ( (This)->lpVtbl -> get_DeferEOLOnHyphen(This,pVal) ) 

#define IHj2Ascii3_put_DeferEOLOnHyphen(This,newVal)	\
    ( (This)->lpVtbl -> put_DeferEOLOnHyphen(This,newVal) ) 

#define IHj2Ascii3_get_CharacterTranslationMode(This,pVal)	\
    ( (This)->lpVtbl -> get_CharacterTranslationMode(This,pVal) ) 

#define IHj2Ascii3_put_CharacterTranslationMode(This,newVal)	\
    ( (This)->lpVtbl -> put_CharacterTranslationMode(This,newVal) ) 

#define IHj2Ascii3_get_IgnoreErrorMessages(This,pVal)	\
    ( (This)->lpVtbl -> get_IgnoreErrorMessages(This,pVal) ) 

#define IHj2Ascii3_put_IgnoreErrorMessages(This,newVal)	\
    ( (This)->lpVtbl -> put_IgnoreErrorMessages(This,newVal) ) 

#define IHj2Ascii3_get_IgnoreNonSource(This,pVal)	\
    ( (This)->lpVtbl -> get_IgnoreNonSource(This,pVal) ) 

#define IHj2Ascii3_put_IgnoreNonSource(This,newVal)	\
    ( (This)->lpVtbl -> put_IgnoreNonSource(This,newVal) ) 

#define IHj2Ascii3_get_SubstituteLigatures(This,pVal)	\
    ( (This)->lpVtbl -> get_SubstituteLigatures(This,pVal) ) 

#define IHj2Ascii3_put_SubstituteLigatures(This,newVal)	\
    ( (This)->lpVtbl -> put_SubstituteLigatures(This,newVal) ) 

#define IHj2Ascii3_get_TranslationStrings(This,pVal)	\
    ( (This)->lpVtbl -> get_TranslationStrings(This,pVal) ) 

#define IHj2Ascii3_get_TagStrings(This,pVal)	\
    ( (This)->lpVtbl -> get_TagStrings(This,pVal) ) 

#define IHj2Ascii3_get_CharacterStrings(This,pVal)	\
    ( (This)->lpVtbl -> get_CharacterStrings(This,pVal) ) 

#define IHj2Ascii3_putSource(This,newVal,bPacked)	\
    ( (This)->lpVtbl -> putSource(This,newVal,bPacked) ) 

#define IHj2Ascii3_FullConvert(This,Output)	\
    ( (This)->lpVtbl -> FullConvert(This,Output) ) 

#define IHj2Ascii3_PartialConvert(This,Output)	\
    ( (This)->lpVtbl -> PartialConvert(This,Output) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IHj2Ascii3_INTERFACE_DEFINED__ */


#ifndef __ICommandList_INTERFACE_DEFINED__
#define __ICommandList_INTERFACE_DEFINED__

/* interface ICommandList */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ICommandList;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F178DE72-F5CD-41D3-A18E-D4016088A82C")
    ICommandList : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveAll( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( 
            /* [in] */ BSTR Command,
            /* [in] */ BSTR Translation) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( 
            /* [in] */ BSTR Command) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Index( 
            /* [in] */ long ival,
            /* [out] */ BSTR *Command,
            /* [out] */ BSTR *Translation) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Translate( 
            /* [in] */ BSTR Command,
            /* [retval][out] */ BSTR *Translation) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICommandListVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICommandList * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICommandList * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICommandList * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICommandList * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICommandList * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICommandList * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICommandList * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            ICommandList * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveAll )( 
            ICommandList * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            ICommandList * This,
            /* [in] */ BSTR Command,
            /* [in] */ BSTR Translation);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            ICommandList * This,
            /* [in] */ BSTR Command);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Index )( 
            ICommandList * This,
            /* [in] */ long ival,
            /* [out] */ BSTR *Command,
            /* [out] */ BSTR *Translation);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Translate )( 
            ICommandList * This,
            /* [in] */ BSTR Command,
            /* [retval][out] */ BSTR *Translation);
        
        END_INTERFACE
    } ICommandListVtbl;

    interface ICommandList
    {
        CONST_VTBL struct ICommandListVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICommandList_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICommandList_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICommandList_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICommandList_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ICommandList_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ICommandList_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ICommandList_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ICommandList_get_Count(This,pVal)	\
    ( (This)->lpVtbl -> get_Count(This,pVal) ) 

#define ICommandList_RemoveAll(This)	\
    ( (This)->lpVtbl -> RemoveAll(This) ) 

#define ICommandList_Add(This,Command,Translation)	\
    ( (This)->lpVtbl -> Add(This,Command,Translation) ) 

#define ICommandList_Remove(This,Command)	\
    ( (This)->lpVtbl -> Remove(This,Command) ) 

#define ICommandList_Index(This,ival,Command,Translation)	\
    ( (This)->lpVtbl -> Index(This,ival,Command,Translation) ) 

#define ICommandList_Translate(This,Command,Translation)	\
    ( (This)->lpVtbl -> Translate(This,Command,Translation) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICommandList_INTERFACE_DEFINED__ */


#ifndef __ITagList_INTERFACE_DEFINED__
#define __ITagList_INTERFACE_DEFINED__

/* interface ITagList */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ITagList;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E6AB49B1-E323-439F-AF43-4D42C981A969")
    ITagList : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveAll( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( 
            /* [in] */ BSTR Tag) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( 
            /* [in] */ BSTR Tag) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Exists( 
            /* [in] */ BSTR Tag) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Index( 
            /* [in] */ long ival,
            /* [out] */ BSTR *Tag) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ITagListVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ITagList * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ITagList * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ITagList * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ITagList * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ITagList * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ITagList * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ITagList * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            ITagList * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveAll )( 
            ITagList * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            ITagList * This,
            /* [in] */ BSTR Tag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            ITagList * This,
            /* [in] */ BSTR Tag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Exists )( 
            ITagList * This,
            /* [in] */ BSTR Tag);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Index )( 
            ITagList * This,
            /* [in] */ long ival,
            /* [out] */ BSTR *Tag);
        
        END_INTERFACE
    } ITagListVtbl;

    interface ITagList
    {
        CONST_VTBL struct ITagListVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ITagList_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ITagList_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ITagList_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ITagList_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ITagList_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ITagList_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ITagList_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ITagList_get_Count(This,pVal)	\
    ( (This)->lpVtbl -> get_Count(This,pVal) ) 

#define ITagList_RemoveAll(This)	\
    ( (This)->lpVtbl -> RemoveAll(This) ) 

#define ITagList_Add(This,Tag)	\
    ( (This)->lpVtbl -> Add(This,Tag) ) 

#define ITagList_Remove(This,Tag)	\
    ( (This)->lpVtbl -> Remove(This,Tag) ) 

#define ITagList_Exists(This,Tag)	\
    ( (This)->lpVtbl -> Exists(This,Tag) ) 

#define ITagList_Index(This,ival,Tag)	\
    ( (This)->lpVtbl -> Index(This,ival,Tag) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ITagList_INTERFACE_DEFINED__ */


#ifndef __ICharacterTranslation_INTERFACE_DEFINED__
#define __ICharacterTranslation_INTERFACE_DEFINED__

/* interface ICharacterTranslation */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ICharacterTranslation;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("01B88613-5213-4562-A56E-56C6B85C3626")
    ICharacterTranslation : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Translation( 
            /* [in] */ long index,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Translation( 
            /* [in] */ long index,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConvertString( 
            /* [in] */ BSTR inString,
            /* [retval][out] */ BSTR *outString) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddPrintables( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICharacterTranslationVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICharacterTranslation * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICharacterTranslation * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICharacterTranslation * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICharacterTranslation * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICharacterTranslation * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICharacterTranslation * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICharacterTranslation * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Translation )( 
            ICharacterTranslation * This,
            /* [in] */ long index,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Translation )( 
            ICharacterTranslation * This,
            /* [in] */ long index,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ConvertString )( 
            ICharacterTranslation * This,
            /* [in] */ BSTR inString,
            /* [retval][out] */ BSTR *outString);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddPrintables )( 
            ICharacterTranslation * This);
        
        END_INTERFACE
    } ICharacterTranslationVtbl;

    interface ICharacterTranslation
    {
        CONST_VTBL struct ICharacterTranslationVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICharacterTranslation_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICharacterTranslation_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICharacterTranslation_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICharacterTranslation_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ICharacterTranslation_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ICharacterTranslation_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ICharacterTranslation_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ICharacterTranslation_get_Translation(This,index,pVal)	\
    ( (This)->lpVtbl -> get_Translation(This,index,pVal) ) 

#define ICharacterTranslation_put_Translation(This,index,newVal)	\
    ( (This)->lpVtbl -> put_Translation(This,index,newVal) ) 

#define ICharacterTranslation_ConvertString(This,inString,outString)	\
    ( (This)->lpVtbl -> ConvertString(This,inString,outString) ) 

#define ICharacterTranslation_AddPrintables(This)	\
    ( (This)->lpVtbl -> AddPrintables(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICharacterTranslation_INTERFACE_DEFINED__ */


#ifndef __ISource_INTERFACE_DEFINED__
#define __ISource_INTERFACE_DEFINED__

/* interface ISource */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ISource;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7A80DA4A-1112-4AD3-9368-D5CF36F78160")
    ISource : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE nextChar( 
            /* [retval][out] */ unsigned char *bVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE close( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE endOfFile( void) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Source( 
            /* [in] */ VARIANT newVal,
            /* [in] */ BOOL bPacked) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_length( 
            /* [retval][out] */ long *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISourceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISource * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISource * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISource * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ISource * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ISource * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ISource * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ISource * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *nextChar )( 
            ISource * This,
            /* [retval][out] */ unsigned char *bVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *close )( 
            ISource * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *endOfFile )( 
            ISource * This);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Source )( 
            ISource * This,
            /* [in] */ VARIANT newVal,
            /* [in] */ BOOL bPacked);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_length )( 
            ISource * This,
            /* [retval][out] */ long *pVal);
        
        END_INTERFACE
    } ISourceVtbl;

    interface ISource
    {
        CONST_VTBL struct ISourceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISource_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ISource_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ISource_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ISource_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ISource_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ISource_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ISource_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ISource_nextChar(This,bVal)	\
    ( (This)->lpVtbl -> nextChar(This,bVal) ) 

#define ISource_close(This)	\
    ( (This)->lpVtbl -> close(This) ) 

#define ISource_endOfFile(This)	\
    ( (This)->lpVtbl -> endOfFile(This) ) 

#define ISource_put_Source(This,newVal,bPacked)	\
    ( (This)->lpVtbl -> put_Source(This,newVal,bPacked) ) 

#define ISource_get_length(This,pVal)	\
    ( (This)->lpVtbl -> get_length(This,pVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ISource_INTERFACE_DEFINED__ */



#ifndef __HJ2ASCIILib_LIBRARY_DEFINED__
#define __HJ2ASCIILib_LIBRARY_DEFINED__

/* library HJ2ASCIILib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_HJ2ASCIILib;

EXTERN_C const CLSID CLSID_HJ2Ascii;

#ifdef __cplusplus

class DECLSPEC_UUID("32B32011-C172-11D2-8BF3-0080ADBA29A6")
HJ2Ascii;
#endif

EXTERN_C const CLSID CLSID_Hj2Ascii3;

#ifdef __cplusplus

class DECLSPEC_UUID("1A34CA4A-E660-4528-86BB-0013E1B988A6")
Hj2Ascii3;
#endif
#endif /* __HJ2ASCIILib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


