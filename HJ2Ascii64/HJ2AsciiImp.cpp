// HJ2AsciiImpl.cpp : Implementation of CHJ2Ascii
#include "stdafx.h"

#ifdef USE_LIBRARY_HPP
#include "library.hpp"
#else
#include <windows.h>
#endif

#include "HJ2Ascii.h"
#include "HJ2AsciiImp.h"
#include "HJ2Ascii3.hpp"

/////////////////////////////////////////////////////////////////////////////
// CHJ2Ascii

STDMETHODIMP cHJ2Ascii::FinalConstruct()
{
	HRESULT hr;

	// Get an interface to the HJConversion interface
	hr = CComCreator<CComObject<cHj2Ascii3> > ::CreateInstance( NULL, IID_IHj2Ascii3, reinterpret_cast<void**>(&m_HJConvert) );
	if (FAILED(hr)) return hr;

	// Get an interface to the command list
	hr = m_HJConvert->get_TranslationStrings( &m_CommandList );
	return hr;

}


STDMETHODIMP cHJ2Ascii::ClearTranslationStrings()
{
	return m_CommandList->RemoveAll();
}

STDMETHODIMP cHJ2Ascii::GetTranslationString(BSTR Command, BSTR* translationString)
{
	HRESULT hr;
	if (translationString == NULL) return E_POINTER;


	// get the index of the specified command
	hr = m_CommandList->Translate( Command, translationString );
	return hr;
}

STDMETHODIMP cHJ2Ascii::SetTranslationString(BSTR Command, BSTR Translation)
{
	return m_CommandList->Add( Command, Translation );
}

STDMETHODIMP cHJ2Ascii::Convert(/*[in]*/ VARIANT Source, /*[in]*/ VARIANT_BOOL bPacked, /*[out, retval]*/ BSTR* ptext)
{
	if (ptext == NULL) return E_POINTER;


	HRESULT hr;
	hr = m_HJConvert->putSource( Source, bPacked );
	if (FAILED(hr)) return hr;

	hr = m_HJConvert->FullConvert( ptext );

	return hr;
}

