// TagList.hpp : Declaration of the cTagList

#ifndef __TAGLIST_H_
#define __TAGLIST_H_

#include "resource.h"       // main symbols
#include "fdt/string.hpp"
#include "fdt/bstring.hpp"
#include "fdt/array.hpp"

/////////////////////////////////////////////////////////////////////////////
// cTagList
class ATL_NO_VTABLE cTagList : 
	public CComObjectRootEx<CComSingleThreadModel>,
//  Not required as tag list not directly accessible to client
//	public CComCoClass<cTagList, &CLSID_TagList>,
	public IDispatchImpl<ITagList, &IID_ITagList, &LIBID_HJ2ASCIILib>
{
public:
	cTagList();

// Not required as TAG LIST is not viewable to client
// IDR_TAGLIST also removed from REGISTRY (see resources view)
// DECLARE_REGISTRY_RESOURCEID(IDR_TAGLIST)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cTagList)
	COM_INTERFACE_ENTRY(ITagList)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// ITagList
public:
	STDMETHOD(Index)(/*[in]*/ long ival, /*[out]*/BSTR* Tag);
	STDMETHOD(Exists)(/*[in]*/BSTR Tag);
	STDMETHOD(Remove)(/*[in]*/BSTR Tag);
	STDMETHOD(Add)(/*[in]*/ BSTR Tag);
	STDMETHOD(RemoveAll)();
	STDMETHOD(get_Count)(/*[out, retval]*/ long* pVal);

private:
	long	m_Count;
	cArray<cString> m_Tags;
};

#endif //__TAGLIST_H_
