// HJ/Hj2ASCII/CommandList.hpp
/**\file
 *\ingroup hj
 * Declaration of cCommandList
 * Copyright 2001 Geac Publishing Systems. All rights reserved
 */

#ifndef __COMMANDLIST_H_
#define __COMMANDLIST_H_

#include "resource.h"       // main symbols
#include "fdt/string.hpp"
#include "fdt/bstring.hpp"
#include "fdt/array.hpp"

 /** The cComandList class is used to specify translations from a source string
 *   to a target string
 */


/////////////////////////////////////////////////////////////////////////////
// cCommandList
class ATL_NO_VTABLE cCommandList : 
	public CComObjectRootEx<CComSingleThreadModel>,
//	public CComCoClass<cCommandList, &CLSID_CommandList>,
	public IDispatchImpl<ICommandList, &IID_ICommandList, &LIBID_HJ2ASCIILib>
{
public:
	cCommandList();

//DECLARE_REGISTRY_RESOURCEID(IDR_COMMANDLIST)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cCommandList)
	COM_INTERFACE_ENTRY(ICommandList)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// ICommandList
public:
	STDMETHOD(Translate)(BSTR Command, BSTR* Translation);
		/**< Get the translation string corresponding to a source string
		 *\param Comand			- The source string
		 *\param Translation	- The corresponding translation
		 */
	STDMETHOD(Index)(/*[in]*/long ival, /*[out]*/ BSTR* Command, /*[out]*/BSTR* Translation);
		/**< Get the source and translation strings stored at a particular index
		 *\param ival			- The index into the translation list
		 *\param Comand			- The source string
		 *\param Translation	- The corresponding translation
		 *\returns Returns S_FALSE if the ival is out of range
		 */

	STDMETHOD(Remove)(/*[in]*/ BSTR Command);
		/**< Removes a source string and its translation from the list of entries
		 *\param Comand			- The source string to be deleted
		 */
	STDMETHOD(Add)(/*[in]*/ BSTR Command, /*[in]*/ BSTR Translation);
		/**< Add the translation string corresponding to a source string
		 *\param Comand			- The source string
		 *\param Translation	- The corresponding translation
		 */
	STDMETHOD(RemoveAll)();
		/**< Delete all entries from the command translation list
		 */
	STDMETHOD(get_Count)(/*[out, retval]*/ long* pVal);
		/**< returns the no of entries in the translation list
		 *\param pVal			- Address of a value to receive the no of entries value
		 */
private:
		struct cCommand {
		cString	Command;
		cString	Translation;
	};

	long	m_Count;
		/**< The no of entries in the translation table
		 */
	cArray<cCommand> m_Commands;
		/**< The array of commands and their translations
		*/

};

#endif //__COMMANDLIST_H_
