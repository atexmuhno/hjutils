// HJ2Ascii.cpp : Implementation of DLL Exports.


// Note: Proxy/Stub Information
//      To build a separate proxy/stub DLL, 
//      run nmake -f HJ2Asciips.mk in the project directory.

#include "stdafx.h"

#ifdef USE_LIBRARY_HPP
#include "library.hpp"
#else
#include <windows.h>
#endif

#include "resource.h"
#include <initguid.h>
#include "HJ2Ascii.h"

#include "HJ2Ascii_i.c"
#include "CommandList.hpp"
#include "TagList.hpp"
#include "HJ2AsciiImp.h"
#include "Hj2Ascii3.hpp"
#include "CharacterTranslation.hpp"
#include "Source.hpp"


CComModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
OBJECT_ENTRY(CLSID_HJ2Ascii, cHJ2Ascii)
//OBJECT_ENTRY(CLSID_HJ2Ascii2, cHJ2Ascii2)
OBJECT_ENTRY(CLSID_Hj2Ascii3, cHj2Ascii3)
// Don't allow client direct access to command list OBJECT_ENTRY(CLSID_CommandList, cCommandList)
// Don't allow client direct access to tag list OBJECT_ENTRY(CLSID_TagList, cTagList)
// Don't allow client direct access to character translation OBJECT_ENTRY(CLSID_CharacterTranslation, cCharacterTranslation)
// Don't allow client direct access to source OBJECT_ENTRY(CLSID_Source, cSource)
END_OBJECT_MAP()

/////////////////////////////////////////////////////////////////////////////
// DLL Entry Point

extern "C"
BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID /*lpReserved*/)
{
    if (dwReason == DLL_PROCESS_ATTACH)
    {
        _Module.Init(ObjectMap, hInstance, &LIBID_HJ2ASCIILib);
        DisableThreadLibraryCalls(hInstance);
    }
    else if (dwReason == DLL_PROCESS_DETACH)
        _Module.Term();
    return TRUE;    // ok
}

/////////////////////////////////////////////////////////////////////////////
// Used to determine whether the DLL can be unloaded by OLE

STDAPI DllCanUnloadNow(void)
{
    return (_Module.GetLockCount()==0) ? S_OK : S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Returns a class factory to create an object of the requested type

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _Module.GetClassObject(rclsid, riid, ppv);
}

/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
    // registers object, typelib and all interfaces in typelib
    return _Module.RegisterServer(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
    return _Module.UnregisterServer(TRUE);
}


