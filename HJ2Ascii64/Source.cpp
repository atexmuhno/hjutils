// Source.cpp : Implementation of cSource
#include "stdafx.h"

#ifdef USE_LIBRARY_HPP
#include "library.hpp"
#else
#include <windows.h>
#endif

#include "HJ2Ascii.h"
#include "Source.hpp"

/////////////////////////////////////////////////////////////////////////////
// cSource
cSource::cSource()
	: m_Length(0),
	  m_Data(NULL),
	  m_Index(0),
	  m_Increment(1)
{}


STDMETHODIMP cSource::nextChar(BYTE* bVal)
{
	if (m_Index >= m_Length)
	{
		*bVal = '\0';
	}
	else
	{
		*bVal = m_Data[m_Index];
		m_Index += m_Increment;
	}
	return S_OK;
}

STDMETHODIMP cSource::close()
{
	// TODO: Add your implementation code here

	return S_OK;
}

STDMETHODIMP cSource::endOfFile()
{
	if (m_Index >= m_Length) 
		return S_OK;
	else return S_FALSE;
}

STDMETHODIMP cSource::put_Source(VARIANT newVal,BOOL bPacked )
{
	// If newVal was passed by reference (automation will do this), dereference it
	if (newVal.vt == (VT_VARIANT | VT_BYREF))
		if (FAILED (::VariantCopyInd(&newVal, &newVal)))
			return AtlReportError(CLSID_HJ2Ascii, _T("Could not dereference source value"), GUID_NULL, 0);

	// Make sure that the variant is of the correct type, byte array.
	if ( newVal.vt != (VT_ARRAY | VT_UI1) )
		return AtlReportError(CLSID_HJ2Ascii, _T("Invalid source data type. It needs to be (VT_ARRAY | VT_UI1)."), GUID_NULL, 0);
		
	// Find the length and data in the variant
	size_t	length =  newVal.parray->rgsabound[0].cElements -  newVal.parray->rgsabound[0].lLbound;
	
	BYTE * pData;
	
	HRESULT hr = SafeArrayAccessData( newVal.parray,(void **) &pData);
	if (FAILED(hr)) 
	{
		return AtlReportError(CLSID_HJ2Ascii, _T("Could not access source data."), GUID_NULL, 0);;
	}

	m_Data.import((BYTE *)pData, length);
	SafeArrayUnaccessData( newVal.parray);

	// Initialization
	m_Index = 0;
	m_Increment = ((bPacked != 0) ? 1 : 2);
	m_Length = length;


	return S_OK;
}

// Return total length
STDMETHODIMP cSource::get_length(long* pVal)
{
	if (pVal == NULL) return E_POINTER;

	*pVal = m_Length / m_Increment;
	return S_OK;
}
