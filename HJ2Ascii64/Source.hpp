// Source.hpp : Declaration of the cSource

#ifndef __SOURCE_H_
#define __SOURCE_H_

#include "resource.h"       // main symbols
#include "assert.h"
#include "fdt/array.hpp"

/////////////////////////////////////////////////////////////////////////////
// cSource
class ATL_NO_VTABLE cSource : 
	public CComObjectRootEx<CComSingleThreadModel>,
//	public CComCoClass<cSource, &CLSID_Source>,
	public IDispatchImpl<ISource, &IID_ISource, &LIBID_HJ2ASCIILib>
{
public:
	cSource();

//DECLARE_REGISTRY_RESOURCEID(IDR_SOURCE)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cSource)
	COM_INTERFACE_ENTRY(ISource)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// ISource
public:
	STDMETHOD(get_length)(/*[out, retval]*/ long* pVal);
	STDMETHOD(put_Source)(/*[in]*/ VARIANT newVal,/*[in]*/ BOOL bPacked );
	STDMETHOD(endOfFile)();
	STDMETHOD(close)();
	STDMETHOD(nextChar)(/*[out]*/ BYTE* bVal);
private:
	long			m_Length;		// Total length of data
	cArray<BYTE>	m_Data;		// Pointer to actual data
	long			m_Index;		// Current index
	long			m_Increment;	// Increment to apply

};

#endif //__SOURCE_H_
