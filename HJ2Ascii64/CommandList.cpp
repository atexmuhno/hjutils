// CommandList.cpp : Implementation of cCommandList
#include "stdafx.h"
#include "HJ2Ascii.h"
#include "CommandList.hpp"

/////////////////////////////////////////////////////////////////////////////
// cCommandList
cCommandList::cCommandList()
{
	m_Count = 0;
}

STDMETHODIMP cCommandList::get_Count(long* pVal)
{
	*pVal = m_Count;
	return S_OK;
}

STDMETHODIMP cCommandList::RemoveAll()
{
	m_Count = 0;
	return S_OK;
}

STDMETHODIMP cCommandList::Add(BSTR Command, BSTR Translation)
{
	m_Commands[m_Count].Command = cSimpleBString( Command );
	m_Commands[m_Count].Command.toLower();
	m_Commands[m_Count].Translation = cSimpleBString( Translation );
	m_Count++;
	return S_OK;
}

STDMETHODIMP cCommandList::Remove(BSTR Command)
{
	cString	temp = cSimpleBString( Command );
	temp.toLower();

	for (long i = 0; i < m_Count; i++)
		if (m_Commands[i].Command == temp) 
		{
			for (long j = i+1; j < m_Count; j++)
				m_Commands[j-1] = m_Commands[j];
			m_Count--;
			return S_OK;
		}
	return S_FALSE;
}


STDMETHODIMP cCommandList::Translate(BSTR Command, BSTR* Translation)
{
	if (Translation == NULL) return E_POINTER;

	cString	temp = cSimpleBString( Command );
	temp.toLower();
	for (long i = 0; i < m_Count; i++)
	{
		cString match = m_Commands[i].Command;
		int	length = match.length();
		int j;
		for (j = 0; j < length; j++ )
		{
			if ( match[j] == '*' )
			{
				// Match on this command
				temp = cSimpleBString( Command );
				int pos = m_Commands[i].Translation.index( 0, '*' );
				if (pos < 0)
				{
					*Translation = cSimpleBString( Command );
				}
				else
				{
					length  = m_Commands[i].Translation.length();
					cString result( m_Commands[i].Translation.leftString(pos) 
							  + temp.subString( j, temp.length() - j)
							  + m_Commands[i].Translation.rightString(length - pos - 1) );
					*Translation = cSimpleBString( result );
				}
				return S_OK;
			}
			if (j >= temp.length()) break;
			if (match[j] != temp[j]) break;
		}

		if ((j >= length) && (j >= temp.length()))
		{
			*Translation = cSimpleBString( m_Commands[i].Translation );
			return S_OK;
		}
	}


	*Translation = cSimpleBString(cString(""));
	return S_FALSE;
}

STDMETHODIMP cCommandList::Index(long ival, BSTR* Command, BSTR* Translation)
{
	if (Command == NULL) return E_POINTER;
	if (Translation == NULL) return E_POINTER;

	if ( (ival < 0) || (ival >= m_Count) ) 
	{
		*Command = cSimpleBString( cString("") );
		*Translation = *Command;
		return S_FALSE;
	}
	*Command = cSimpleBString( m_Commands[ival].Command );
	*Translation = cSimpleBString( m_Commands[ival].Translation );
	return S_OK;
}
