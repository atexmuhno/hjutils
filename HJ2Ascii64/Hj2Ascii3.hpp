// HJ/Hj2ASCII/Hj2Ascii3.hpp
/**\file
 *\ingroup hj
 * Declaration of cHj2Ascii3
 * Copyright 2001 Geac Publishing Systems. All rights reserved
 */

#ifndef __HJ2ASCII3_H_
#define __HJ2ASCII3_H_

#include "resource.h"       // main symbols
#include "fdt/string.hpp"

typedef unsigned char hjchar;			// A definition for characters in a H&J stream

// Structure of H&J Controls as read by H&J Parser
typedef struct cHJControl {
		hjchar	code;					// The H&J control code
		unsigned char	params[20];		// The parameters associated with the code
		cString			text;			// or the string associated with the code
	} cHJControl;

/////////////////////////////////////////////////////////////////////////////
// cHj2Ascii3
class ATL_NO_VTABLE cHj2Ascii3 : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cHj2Ascii3, &CLSID_Hj2Ascii3>,
	public IDispatchImpl<IHj2Ascii3, &IID_IHj2Ascii3, &LIBID_HJ2ASCIILib>
{
public:
	cHj2Ascii3();


DECLARE_REGISTRY_RESOURCEID(IDR_HJ2ASCII3)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cHj2Ascii3)
	COM_INTERFACE_ENTRY(IHj2Ascii3)
	COM_INTERFACE_ENTRY2(IDispatch,IHj2Ascii3)
END_COM_MAP()

// IHj2Ascii3
public:
	STDMETHOD(PartialConvert)(/*[out]*/BSTR* Output);
		/**< Converts a source string of H&J'd text to ascii
		 *\param Output			- Address of the returned Ascii string
								  A partial conversion extends from one tag in a set of tags
								  to the next tag
		 *\returns Returns S_FALSE if end of input stream is seen before a match on any defined tags
		 */
	STDMETHOD(FullConvert)(/*[out]*/BSTR* Output);
		/**< Converts a source string of H&J'd text to ascii
		 *\param Output			- Address of the returned Ascii string
								  The entire source is converted
		 */
	STDMETHOD(get_CharacterStrings)(/*[out, retval]*/ ICharacterTranslation** pVal);
		/**< Returns an interface to the character translation table. 
		 *   The default value of the character translation table is empty (all translations are null)
		 *\param pVal			- Where interface address is returned to
		 */

	STDMETHOD(putSource)(/*[in]*/ VARIANT Source,/*[in]*/ BOOL bPacked);
		/**< Specifies the address of a H&J'd text to be converted by a subsequent 
			 PartialConvert of FullConvert
		 *\param Source			- The source string to be converted
		 *\param bPacked		- true if the source string is packed 1 char per byte
								- false if the string is packed 1 char per 2 bytes
		 */
	STDMETHOD(get_TagStrings)(/*[out, retval]*/ ITagList** pVal);
		/**< Returns an interface to the list of tags that delimit output in a PartialConvert
		 *   The default value of the tags table is empty (i.e. there are no tags defined)
		 *\param pVal			- Where interface address is returned to
		 */

	STDMETHOD(get_TranslationStrings)(/*[out, retval]*/ ICommandList ** pVal);
		/**< Returns an interface to the list of command translation strings to be used
		 *   when converting H&J'd text to Ascii
		 *   The default value of the comamnd translations table is empty 
		 *   (i.e. there are no command translations defined)
		 *\param pVal			- Where interface address is returned to
		 */

	STDMETHOD(get_SubstituteLigatures)(/*[out, retval]*/ BOOL* pVal);
		/**< Gets the value of the SubstitueLigatures property
		 *		true to ignore source text between CM_LIS and CM_LIE
		 *      false to output text between CM_LIS and CM_LIE
		 *      The default value is false
		 *\param pVal			- Where property value is returned to
		 *\see put_SubstituteLigatures
		 */
	STDMETHOD(put_SubstituteLigatures)(/*[in]*/ BOOL newVal);
		/**< Sets the value of the SubstitueLigatures property
		 *\param pVal			- Where property value is returned to
		 *\see gut_SubstituteLigatures
		 */
	STDMETHOD(get_IgnoreNonSource)(/*[out, retval]*/ BOOL* pVal);
		/**< Gets the value of the IgnoreNonSource property
		 *		true to ignore non source text (text inserted by H&J)
		 *      false to output non source text
		 *	    The default value is false
		 *\param pVal			- Where property value IgnoreNonSource is returned to
		 *\see put_IgnoreNonSource
		 */
	STDMETHOD(put_IgnoreNonSource)(/*[in]*/ BOOL newVal);
		/**< Sets the value of the IgnoreNonSource property
		 *\param pVal			- Where property value IgnoreNonSource is set from
		 *\see put_IgnoreNonSource
		 */
	STDMETHOD(get_IgnoreErrorMessages)(/*[out, retval]*/ BOOL* pVal);
		/**< Gets the value of the IgnoreErrorMessages property
		 *		true to ignore H&J error messages
		 *      false to output H&J error messages
		 *      the default value is true
		 *\param pVal			- Where property value IgnoreErrorMessages is returned to
		 *\see put_IgnoreErrorMessages
		 */
	STDMETHOD(put_IgnoreErrorMessages)(/*[in]*/ BOOL newVal);
		/**< Sets the value of the IgnoreErrorMessages property
		 *		true to ignore H&J error messages
		 *      false to output H&J error messages
		 *\param pVal			- Where property value IgnoreErrorMessages is set from
		 *\see get_IgnoreErrorMessages
		 */
	STDMETHOD(get_CharacterTranslationMode)(/*[out, retval]*/ long* pVal);
		/**< Gets the value of the CharacterTranslationMode property
		 *		0 to output all characters as they are (without translation)
		 *		1 to only output printable Ascii characters
		 *		2 to output characters after passing them through the character translation table
		 *      The default value is 0
		 *\param pVal			- Where property value CharacterTranslationMode is returned to
		 *\see put_CharacterTranslationMode
		 */
	STDMETHOD(put_CharacterTranslationMode)(/*[in]*/ long newVal);
		/**< Puts the value of the CharacterTranslationMode property
		 *\param pVal			- Where property value CharacterTranslationMode is set from
		 *\see gut_CharacterTranslationMode
		 */
	STDMETHOD(get_DeferEOLOnHyphen)(/*[out, retval]*/ BOOL* pVal);
		/**< Gets the value of the DeferEOLOnHyphen property
		 *		true to output an end of line string on the first space after a non source hyphen
		 *		false to take no special notice o fnon source hyphens
		 *      The default value is false
		 *\param pVal			- Where property value DeferEOLOnHyphen is returned to
		 *\see put_DeferEOLOnHyphen
		 */
	STDMETHOD(put_DeferEOLOnHyphen)(/*[in]*/ BOOL newVal);
		/**< Puts the value of the DeferEOLOnHyphen property
		 *\param pVal			- Where property value DeferEOLOnHyphen is returned to
		 *\see put_DeferEOLOnHyphen
		 */
	STDMETHOD(get_EOLString)(/*[out, retval]*/ BSTR* pVal);
		/**< Gets the value of the EOLString property, which is the string output when  a H&J
		 *    end of line code is encountered
		 *      The default value is "\n"
		 *\see put_EOLString
		 */
	STDMETHOD(put_EOLString)(/*[in]*/ BSTR newVal);
		/**< Puts the value of the EOLString property, which is the string output when  a H&J
		 *    end of line code is encountered
		 *\see put_EOLString
		 */

	STDMETHOD(FinalConstruct)();
private:
	cString			m_EOLString;
		/**< The string to be output on H&J end of line
		 */
	bool			m_DeferEOLOnHyphen;
		/**< true to defer an end of line when encountering a non source hyphen
		*/
	bool			m_IgnoreNonSource;
		/**< true to ignore non source text inserted by H&J
		*/
	bool			m_IgnoreErrorMessages;
		/**< true to not output H&J error messages
		*/
	bool			m_SubstituteLigatures;
		/**< true to convert ligatures to their corresponding code
		*/
	enum cCharacterMode { cmAsIs, cmPrintableOnly, cmTranslationTable };
	cCharacterMode	m_CharacterTranslationMode;
		/**< The type of character translation to be performed on output
		*/
	CComPtr<ICommandList> m_Commands;
		/**< The interface to the list of command translations to be performed on output
		*/
	CComPtr<ITagList>	m_Tags;
		/**< The interface to the list of tag strings that delimit partial output
		*/
	CComPtr<ICharacterTranslation> m_CharMap;
		/**< The interface to character translation map
		*/
	CComPtr<ISource>	m_Source;

	void			putSourceChar( hjchar ch );
		/**< Outputs a (not yet translated) character to the output stream
		 *\param ch			- teh character to be output
		 */
	void			putChar(hjchar ch);
		/**< Outputs a translated character to the output stream
		 *\param ch			- the character to be output
		 */
	void			putString( cString s);
		/**< Outputs a (not yet translated) character string to the output stream
		 *\param s			- the character stream to be output
		 */
	void			parsehj();
		/**< Method that parses H&J source string and produces an output stream
		 */
	hjchar			peek(void);
		/**< Method that returns without consuming the next character in the input stream
		 */
	hjchar			nextChar(void);
		/**< Method that returns the next character in the input stream
		 */
	cString			getTextString(  bool (*termination)(hjchar ch)  );
		/**< Method that returns a sequence of characters from the input source stream
		 * \param termination	-- Method that returns true if the next character in the input
		 *                         stream will terminate the sequence of characters
		 */
	bool			putControl(cHJControl Control );
		/**< Method that generates appropriate code to correspond to the H&J control specified
		 * \param Control	- The parameters to the H&J control code
		 * \returns	true if the parsing of the input stream should terminate		
		 */
	hjchar			m_ch;
		/**< A one character buffer o the next character to be read from the input stream
		*/
	bool			m_held;
		/**< true if the one character input stream buffer is full
		*/
	bool			m_Note;
		/**< true if the current H*J command string is a note of some description
		*/
	long			m_NonSourceCount;
		/**< non zero when processing non source (H&J inserted characters)
		*/
	cString			m_Output;
		/**< where converted data is delivered to
		*/
	bool			m_ligature;
		/**< true if the previous H&J command was a CM_LIS
		*/
	bool			m_DeferEOL;
		/**< true if end of line is to be deferred to the next space
		*/
	bool			m_skip;
		/**< true to ignore (nearly) everything
		*/

	enum cTagsMode { tmIgnoreTags, tmTagInProgress, tmSkipToTag };
	cTagsMode		m_TagsMode;
		/**< true if end of line is to be deferred to the next space
		*/
	cString			m_PrevCommand;
		/**< Stored 1st part of a 2 part command *com,*not,*jmp,*nts
		*/

};

#endif //__HJ2ASCII3_H_
