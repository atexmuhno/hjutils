// TagList.cpp : Implementation of cTagList
#include "stdafx.h"

#ifdef USE_LIBRARY_HPP
#include "library.hpp"
#else
#include <windows.h>
#endif

#include "HJ2Ascii.h"
#include "TagList.hpp"

/////////////////////////////////////////////////////////////////////////////
// cTagList
cTagList::cTagList() : m_Count(0){}


STDMETHODIMP cTagList::get_Count(long* pVal)
{
	if (pVal == NULL) return E_POINTER;

	*pVal = m_Count;
	return S_OK;
}

STDMETHODIMP cTagList::RemoveAll()
{
	m_Count = 0;
	return S_OK;
}

STDMETHODIMP cTagList::Add(BSTR Tag)
{
	m_Tags[m_Count] = cSimpleBString( Tag );
	m_Tags[m_Count].toLower();
	m_Count++;
	return S_OK;
}

STDMETHODIMP cTagList::Remove(BSTR Tag)
{
	cString	temp = cSimpleBString( Tag );
	temp.toLower();

	for (long i = 0; i < m_Count; i++)
		if (m_Tags[i] == temp) 
		{
			for (long j = i+1; j < m_Count; j++)
				m_Tags[j-1] = m_Tags[j];
			m_Count--;
			return S_OK;
		}
	return S_FALSE;
}

STDMETHODIMP cTagList::Exists(BSTR Tag)
{

	cString	temp = cSimpleBString( Tag );
	temp.toLower();
	int length = temp.length();
	for (long i = 0; i < m_Count; i++)
	{
		cString match = m_Tags[i];
		int taglen = match.length();
		int j;
		for (j = 0; j < taglen; j++)
		{
			if (match[j] == '*') return S_OK;
			if (j > length) break;
			if (match[j] != temp[j]) break;
		}
		if ((j == taglen) && (j == length)) return S_OK;
	}

	return S_FALSE;
}

STDMETHODIMP cTagList::Index(long ival, BSTR* Tag)
{
	if (Tag == NULL) return E_POINTER;

	if ( (ival < 0) || (ival >= m_Count) ) return S_FALSE;
	*Tag = cSimpleBString( m_Tags[ival] );
	return S_OK;
}
