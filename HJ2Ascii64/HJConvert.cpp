// HJCOnvert.cpp : Do actual conversion of H&J'd binary
#include "stdafx.h"

#ifdef USE_LIBRARY_HPP
#include "library.hpp"
#else
#include <windows.h>
#endif

#include "HJ2Ascii.h"
#include "HJ2Ascii3.hpp"
#include "hj/hjcore/chcodes.h"
#include "fdt/bstring.hpp"

typedef unsigned char hjchar;
   
// This table specifies the no of bytes of parameter that follows a H&J binary control
const int ControlLength[] =
{
	0,1,2,3,4,5,6,7,	//200-207
	2,0,0,2,0,0,0,0,	//210-217
	2,0,0,0,0,0,0,0,	//220-227
	0,0,0,0,0,0,0,0, 	//230-237
	2,0,2,0,0,0,0,0,	//240-247
	0,0,0,0,0,2,2,2,	//250-257
	2,0,0,0,0,0,0,0,	//260-267
	2,2,2,2,0,0,0,2,	//270-277
	2,0,0,0,0,0,0,0,	//300-307 
	0,0,0,0,1,0,0,0,	//310-317 
	2,0,0,0,0,0,0,0,	//320-327
	4,8,0,0,0,0,0,0,	//330-337
	2,0,0,0,0,0,0,0,	//340-347
	16,0,0,0,0,0,0,0,	//350-357
	2,0,1,0,2,0,0,0,	//360-367
	0,0,0,0,0,0,0,0		//370-377 
};

// return true if the character terminates a H&J command string
bool
checkEndCommand(hjchar ch)
{
	return (ch == CM_EC1) || (ch == CM_EC2) || (ch == CM_EC5) || (ch == CM_EC6) || (ch == '\0');
}

// return true if the character terminates a H&J error message
bool 
checkEndError(hjchar ch)
{
	return (ch == CM_EEM) || (ch == '\0');
}

// return true if the character terminates a ligature sequence
bool 
checkEndLigature(hjchar ch)
{
	return (ch == CM_LIE) || (ch == '\0');
}

// Output a character to the output stream
void
cHj2Ascii3::putChar( hjchar ch )
{
	if (m_TagsMode != tmSkipToTag) m_Output += ch;
}

void cHj2Ascii3::putSourceChar(hjchar ch)
{
	if ((ch == ' ') && (m_DeferEOL))
	{
		m_DeferEOL = false;
		putString( m_EOLString );
	}
	if (m_NonSourceCount > 0)
	{
		if ((ch == CM_HYP) && m_DeferEOLOnHyphen)
		{
			m_DeferEOL = true;
			return;
		}
		else
			if (m_IgnoreNonSource) return;
	}
	switch (m_CharacterTranslationMode)
	{
	case cmPrintableOnly:
		 if ((ch < ' ') || (ch > 127)) return;
		 // and drop through to print the character
	case cmAsIs:
		 putChar( ch );
		 break;
	
	default:
		if (m_TagsMode != tmSkipToTag)
		{
			BSTR conversion;
			m_CharMap->get_Translation( static_cast<long>(ch), &conversion );
			m_Output += cSimpleBString( conversion );
		}
	}
}

// Output a string to the output stream
void 
cHj2Ascii3::putString( cString s )
{
	if (m_TagsMode != tmSkipToTag) m_Output.append( s );
}

// Peek at the next character in the input stream
hjchar
cHj2Ascii3::peek()
{
	if (m_held) 
		return m_ch;
	else
	{
		m_Source->nextChar( &m_ch);
		m_held = true;
		return m_ch;
	}
}


// Returns the next printable character in the input stream
// Non printable characters are dispatched via putControl
hjchar
cHj2Ascii3::nextChar(void)
{
	hjchar ch;
	cHJControl	Control;

	while (true)
	{
		ch = peek();
		m_held = false;

		if(ch < 0x80 ) 
		{
			if (!m_skip) return ch;			// Return if printable
		}
		else
		{

			// Gather the parameters for the control
			Control.code = ch;
			for (int i = 0; i < ControlLength[ch & 0x7f]; i++)
				m_Source->nextChar( &Control.params[i] );

			// Process the control
			if (putControl( Control )) return '\0';
		}
	};
}

// Builds up a string of characters from the text stream
cString
cHj2Ascii3::getTextString(  bool (*termination)(hjchar ch)  )
{
	// Take a guess at string size
	cString result(100);

	while (!(*termination)(peek()))
	{
		result += nextChar();
	}
	return result;
}

// Process a H&J control code and all of its parameters
// Returns true if processinbg of this control is to terminate input
bool
cHj2Ascii3::putControl(cHJControl Control )
{
	HRESULT hr=0;		// To prevent compiler warnings
	cString mnemonic;	// To prevent compiler warnings
	bool	isCommand;	// Indicate it's really a command
	bool	result;

	if ((Control.code == CM_DC3) || (!m_skip))
	{
	switch (Control.code)
	{
		// Handle H&J commands
		// Standard format is
		//   <bc >*tftimes<ec1>
		// For notes and comments, the format is
		//	 <bc >*nts t<ec1><bc ><bn ><dc1 055>his is a note <ec1><bc >*nte<ec1>
		//	 <bc >*not<ec1><bc ><bn ><dc1 055>a: this is a note<10><ec1>
		//	 <bc >*com<ec1><bc ><bn ><dc1 055> this is a comment<10><ec1>
		//	 <bc >*jmp<ec1><bc ><bn ><dc1 055> this is a comment<10><ec1>

	case CM_BC1: 

			Control.text = getTextString( &checkEndCommand );

			// BC1 has a "peeked" terminator, unpeek it"
			m_held = false;

			// Strip off optional trailing EOC characters
			Control.text.stripTrailing( static_cast<char>(CM_EOC) );

			isCommand = (Control.text[0] == CM_CMD);
			Control.text.stripLeading( static_cast<char>(CM_CMD) ); 

			if(!isCommand)
			{
				if (m_PrevCommand == "")
				{
					return false;
				}

				Control.text.prepend( m_PrevCommand );
			}
			else
			{
				// Handle special H&J commands to be postponed
				mnemonic = Control.text.leftString(3);
				mnemonic.toLower();
				if (   (mnemonic == "nts")
					|| (mnemonic == "not")
					|| (mnemonic == "com") 
					|| (mnemonic == "jmp")
					|| (mnemonic == "sfr")
					)
				{
					m_PrevCommand = Control.text;
					return false;
				}
			}
			m_PrevCommand = "";

			// Pre-translate command string
			BSTR	translation;
			switch (m_CharacterTranslationMode)
			{
			case cmTranslationTable:
				m_CharMap->ConvertString( cSimpleBString(Control.text), &translation );
				Control.text = cSimpleBString( translation );
				break;

			case cmPrintableOnly:
				{
					int j = 0;
					for (int i = 0; i < Control.text.length(); i++)
						if ((Control.text[i] >= ' ') && (Control.text[i] < 128))
						{
							if (j < i) Control.text[j] = Control.text[i];
							j++;
						}
					if (j < Control.text.length()) Control.text = Control.text.leftString(j);
				}
				
			}

			result = false;
			// See if command is tag...
			if (m_TagsMode != tmIgnoreTags)
			{
				if (mnemonic == cString("tag") )
				{
					// Does this tag match one that's expected ?
					cString temp = Control.text.rightString( Control.text.length() - 3 );
					hr = m_Tags->Exists( cSimpleBString(temp) );
					if (hr == S_OK)
					{
						// If looking for this tag, then it's been found
						if (m_TagsMode == tmSkipToTag)
							m_TagsMode = tmTagInProgress;
						else
							// Consecutive identical tags, leave mode as TagInprogress
							// So that it is picked up on next entry
							result = true;
					}
					else
					{
						if (m_TagsMode == tmTagInProgress)
						{
							m_TagsMode = tmSkipToTag;
							result = true;
						}

					}
				}
			}

			// Attempt to translate command to output format
			hr = m_Commands->Translate( cSimpleBString(Control.text), &translation );
			if (hr == S_OK) putString( cString( cSimpleBString( translation) ) );

			return result;
			break;

	case CM_LIS: 
			Control.text = getTextString( &checkEndLigature );
			if (!m_SubstituteLigatures)
			{
				for (int i = 0; i < Control.text.length(); i++)
					putSourceChar( Control.text[i] );
			}
			m_ligature = true;
			break;

	case CM_BEM: 
			Control.text = getTextString( &checkEndError );

			// BEM has a "peeked" terminator, unpeek it"
			m_held = false;
			if (!m_IgnoreErrorMessages)
			{
				for (int i = 0; i < Control.text.length(); i++)
					putSourceChar( Control.text[i] );
			}
			break;

	case CM_RTN:
			if (!m_DeferEOL) putString( m_EOLString );
			break;

	case CM_NSC:
			m_NonSourceCount++;
			break;

	case CM_SRC:
			m_NonSourceCount--;
			break;

	case CM_BN:
			m_Note = true;
			break;

	case CM_LIE:
			if ( m_SubstituteLigatures  && m_ligature)
				putSourceChar( Control.params[0] );
			m_ligature = false;
			break;

	case CM_DC3:
			if (Control.params[0] == 3)
			{
				m_skip = Control.params[1] == 0;
			}
			break;
	}
	}
	return false;
}

// Parse H&J text and convert to an output stream
void
cHj2Ascii3::parsehj(void)
{
	hjchar	ch;

	// Get and output each character
	// H&J controls are processed within nextChar
	while (true)
	{
		ch = nextChar();
		if (ch == '\0') break;
		else putSourceChar(ch);
	}

}


