// HJ2AsciiImpl.hpp : Declaration of the CHJ2Ascii

#ifndef __HJ2ASCII_H_
#define __HJ2ASCII_H_

#include "resource.h"       // main symbols
#include "HJ2Ascii3.hpp"
#include "CommandList.hpp"

 /** The cHJ2Ascii class is used to convert H&J'd text to ascii, allowing for
 *   limited translation of commands
 *   This class exists for backwards compatibilty only, it simply uses facilities
 *    provided by the cHj2Ascii3 class
 */

/////////////////////////////////////////////////////////////////////////////
// cHJ2Ascii
class ATL_NO_VTABLE cHJ2Ascii : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cHJ2Ascii, &CLSID_HJ2Ascii>,
	public IDispatchImpl<IHJ2Ascii2, &IID_IHJ2Ascii2, &LIBID_HJ2ASCIILib>
{
public:
	cHJ2Ascii()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_HJ2ASCII)
DECLARE_NOT_AGGREGATABLE(cHJ2Ascii)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cHJ2Ascii)
	COM_INTERFACE_ENTRY(IHJ2Ascii2)
	COM_INTERFACE_ENTRY(IHJ2Ascii)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IHJ2Ascii
public:

	STDMETHOD(SetTranslationString)(/*[in]*/ BSTR Command, /*[in]*/ BSTR Translation);
		/**< Add the translation string corresponding to a source string
		 *\param Comand			- The source string
		 *\param Translation	- The corresponding translation
		 */

	STDMETHOD(GetTranslationString)(/*[in]*/ BSTR  Command, /*[out,retval]*/BSTR* translationString);
		/**< Get the translation string corresponding to a particular source string
		 *\param Comand			- The source string
		 *\param Translation	- The corresponding translation
		 */

	STDMETHOD(ClearTranslationStrings)();
		/**< Delete all entries from the command translation list
		 */

	STDMETHOD(Convert)(/*[in]*/ VARIANT Source, /*[in]*/ VARIANT_BOOL bPacked, /*[out]*/ BSTR *ptext);
		/**< Converts a source string of H&J'd text to ascii
		 *\param Source			- The source string to be converted
		 *\param bPacked		- true if the source string is packed 1 char per byte
								- false if the string is packed 1 char per 2 bytes
		 *\param ptext			- Address of the returned Ascii string
		 */

	STDMETHOD(FinalConstruct)();
private:
	CComPtr<IHj2Ascii3>		m_HJConvert;
		/**<interface to the HJ to Ascii conversion routine
		 */

	CComPtr<ICommandList>	m_CommandList;
		/**< Interface to the command translation list
		 */

};

#endif //__HJ2ASCII_H_
