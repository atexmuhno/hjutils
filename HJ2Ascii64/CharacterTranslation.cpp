// CharacterTranslation.cpp : Implementation of cCharacterTranslation
#include "stdafx.h"

#ifdef USE_LIBRARY_HPP
#include "library.hpp"
#else
#include <windows.h>
#endif

#include "HJ2Ascii.h"
#include "CharacterTranslation.hpp"
#include "fdt/bstring.hpp"
//_bstr

/////////////////////////////////////////////////////////////////////////////
// cCharacterTranslation
STDMETHODIMP cCharacterTranslation::get_Translation(/*[in]*/long ival, /*[out]*/ BSTR *Command )
{
	if (Command == NULL) return E_POINTER;

	if ((ival >= 0) && (ival <= 255))
	{
		*Command = cSimpleBString(m_map[ival]);
		return S_OK;
	}
	else
	{
		*Command = cSimpleBString( cString("") );
		return S_FALSE;
	}
}

STDMETHODIMP cCharacterTranslation::put_Translation(/*[in]*/long ival, /*[in]*/  BSTR Command )
{
	if ((ival >= 0) && (ival <= 255))
	{
		m_map[ival] =  cSimpleBString( Command );
		return S_OK;
	}
	else
		return S_FALSE;
}

STDMETHODIMP cCharacterTranslation::ConvertString(BSTR inString, BSTR *outString)
{
	if(outString == NULL) return E_POINTER;

	cString input = cSimpleBString( inString );
	cString output(100);
	for (int i = 0; i < input.length(); i++ )
		output += m_map[input[i]];
	*outString = cSimpleBString( output );
	return S_OK;
}
	
STDMETHODIMP cCharacterTranslation:: AddPrintables()
{
	char	str[2];

	for (int i = 32; i < 127; i++ )
	{
		str[0] = i;
		str[1] = '\0';
		m_map[i] = cString( str );
	}
	return S_OK;
}
