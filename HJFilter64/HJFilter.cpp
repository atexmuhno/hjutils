// HJFilter.cpp : Implementation of DLL Exports.


// Note: Proxy/Stub Information
//      To build a separate proxy/stub DLL, 
//      run nmake -f HJFilterps.mk in the project directory.

#include "stdafx.h"
#include "resource.h"
#include <initguid.h>
#include "HJFilter.h"

#include "FilterObj.h"
#include "filtreg.hxx"

CComModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
	OBJECT_ENTRY(__uuidof(CFilterObj), CFilterObj)
END_OBJECT_MAP()

WCHAR wszLongPath[256];
WCHAR wszShortPath[256];

/////////////////////////////////////////////////////////////////////////////
// DLL Entry Point

extern "C"
BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID /*lpReserved*/)
{
    if (dwReason == DLL_PROCESS_ATTACH)
    {
        _Module.Init(ObjectMap, hInstance, NULL);
        DisableThreadLibraryCalls(hInstance);
    }
    else if (dwReason == DLL_PROCESS_DETACH)
        _Module.Term();

	::GetModuleFileNameW(hInstance, wszLongPath, sizeof(wszLongPath)/sizeof(wszLongPath[0]));
	::GetShortPathNameW(wszLongPath, wszShortPath, sizeof(wszShortPath)/sizeof(wszShortPath[0]));

    return TRUE;    // ok
}

/////////////////////////////////////////////////////////////////////////////
// Used to determine whether the DLL can be unloaded by OLE

STDAPI DllCanUnloadNow(void)
{
    return (_Module.GetLockCount()==0) ? S_OK : S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Returns a class factory to create an object of the requested type

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _Module.GetClassObject(rclsid, riid, ppv);
}

/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

/*
STDAPI DllRegisterServer(void)
{
    // registers object, typelib and all interfaces in typelib
    return _Module.RegisterServer(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
    return _Module.UnregisterServer(TRUE);
}
*/



//-------------------------------------------------------------------------
//
//  Function:   DllRegisterServer
//              DllUnregisterServer
//
//  Summary:    Registers and unregisters DLL server
//
//              The registration procedure uses a set of macros
//              developed for use within the Indexing Service code.
//              The macros are in the included filtreg.hxx.
//
//  Returns:    DllRegisterServer
//                  S_OK
//                      Registration was successful
//                  SELFREG_E_CLASS
//                      Registration was unsuccessful
//                  SELFREG_E_TYPELIB
//                      (not implemented)
//                  E_OUTOFMEMORY
//                      (not implemented)
//                  E_UNEXPECTED
//                      (not implemented)
//              DllUnregisterServer
//                  S_OK
//                      Unregistration was successful
//                  S_FALSE
//                      Unregistration was successful, but other
//                      entries still exist for the DLL's classes
//                  SELFREG_E_CLASS
//                      (not implemented)
//                  SELFREG_E_TYPELIB
//                      (not implemented)
//                  E_OUTOFMEMORY
//                      (not implemented)
//                  E_UNEXPECTED
//                      (not implemented)
//
//--------------------------------------------------------------------------

//+-------------------------------------------------------------------------
//
//  These structures define the registry keys for the registration process.
//
//--------------------------------------------------------------------------

SClassEntry const ahjClasses[] =
{
	{
		L".hj",
		L"HJFilter.Document",
		L"HJ Filter Document",
		L"{8E2F5687-C39B-41A4-90BA-89F7C9A3161D}",
		L"HJ Filter Document"
	}
};

SHandlerEntry const hjHandler =
{
	L"{8E2F5688-C39B-41A4-90BA-89F7C9A3161D}",
	L"HJ Filter Persistent Handler",
	L"{8E2F5685-C39B-41A4-90BA-89F7C9A3161D}"	// CLSID_CFilterObj
};

SFilterEntry const hjFilter =
{
	L"{8E2F5685-C39B-41A4-90BA-89F7C9A3161D}",	// CLSID_CFilterObj
	L"HJ Filter",
	wszShortPath,
	L"Both"
};

//+-------------------------------------------------------------------------
//
//  This macro defines the registration/unregistration routines for the DLL.
//
//--------------------------------------------------------------------------

DEFINE_DLLREGISTERFILTER(hjHandler, hjFilter, ahjClasses )
