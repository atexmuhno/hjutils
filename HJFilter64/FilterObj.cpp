// FilterObj.cpp : Implementation of CFilterObj
#include "stdafx.h"
#include "HJFilter.h"
#include "FilterObj.h"

#include <sys\timeb.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>

GUID const guidStorage = PSGUID_STORAGE;    // GUID for storage property set

#define LOG_VER "1.06.01"

/////////////////////////////////////////////////////////////////////////////
#define BUF_LEN 2048

#ifdef _DIAGREL
char* w2c(LPCWSTR pws)
{
	int nLen = wcslen(pws);
	char* pc = (char*) malloc(nLen + 1);
	for (int i = 0; i <= nLen; i++)
	{
		pc[i] = (char) pws[i];
	}

	return pc;
}

void Log(LPCTSTR szFmt, ...)
{
	va_list args;
	va_start(args, szFmt);
	TCHAR buf[BUF_LEN];

	struct _timeb	nowx;
	_ftime(&nowx);
	struct tm *now = localtime(&nowx.time);

	_sntprintf(buf, BUF_LEN, LOG_VER " %02d/%02d/%02d %02d:%02d:%02d.%03d %5d - ",
		now->tm_year % 100, now->tm_mon + 1, now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec, nowx.millitm,
		GetCurrentThreadId());

	int len = strlen(buf);
	_vsntprintf(buf + len, BUF_LEN - len, szFmt, args);

	// Add Cr/Lf
	buf[BUF_LEN - 3] = '\0';
	int n = strlen(buf);
	buf[n + 0] = '\r';
	buf[n + 1] = '\n';
	buf[n + 2] = '\0';

	va_end(args);

	OutputDebugString(buf);

	HANDLE hMutex;
	if ((hMutex = CreateMutex(NULL, FALSE, _T("HjFilterMutex"))) == NULL)
	{
		return;
	}

	if (WaitForSingleObject(hMutex, INFINITE) == WAIT_OBJECT_0)
	{
		int fd = _open(_T("c:\\logs\\hjfilter.log"), _O_CREAT | _O_WRONLY | _O_APPEND, _S_IREAD | _S_IWRITE);

		if (fd != -1)
		{
			_write(fd, buf, strlen(buf));
			_close(fd);
		}

		ReleaseMutex(hMutex);
		CloseHandle(hMutex);
	}
}

void Dump(void *data, int dataLength, bool bWide)
{
	int bytesRemaining = dataLength;
    int bytesPerRow = 16;
	char *ptr = (char *)data;

	Log("  (Packet size = %d)", dataLength);

	while(bytesRemaining > 0)
    {
    	TCHAR	szRowHex[512]		= "";
        TCHAR	szRowAscii[512]		= "";

    	// row in 0..(bytesPerRow-1)
        for(int row=0; row < bytesPerRow; row++)
        {
        	// put space before hex on rows after first
        	if (row > 0)
            {
            	strcat(szRowHex, " ");
            }

        	if (bytesRemaining > 0)
            {
	        	// get the hex form of the byte
		        char h[10];
				sprintf(h, "%02X", (unsigned char) *ptr);
            	strcat(szRowHex, h);

				if (!bWide || (row & 1) == 0)
				{
					char t[10];
					if (isprint(*ptr) && !iscntrl(*ptr))
					{
						sprintf(t, "%c", *ptr);
					}
					else
					{
						sprintf(t, "%c", '.');
					}

            		strcat(szRowAscii, t);
				}

                ptr++;
                bytesRemaining--;
            }
            else
            {
            	strcat(szRowHex, "  ");
            	strcat(szRowAscii, " ");
            }
        }

       Log("%s | %s", szRowHex, szRowAscii);
    } // while
}
#endif

/////////////////////////////////////////////////////////////////////////////
// CFilterObj

CFilterObj::CFilterObj()
	: m_pStm(NULL)
	, m_nChunkId(1)
	, m_hFile(INVALID_HANDLE_VALUE)
    , m_bContents(false)
{
#ifdef _DIAGREL
	Log("CFilterObj() - constructor in");
#endif

	m_wszBuffer = NULL;
	*m_wszFilename = 0;

#ifdef _DIAGREL
	Log("CFilterObj() - constructor out");
#endif
}

CFilterObj::~CFilterObj()
{
#ifdef _DIAGREL
	Log("CFilterObj() - desstructor in");
#endif

	CleanUp();

#ifdef _DIAGREL
	Log("CFilterObj() - desstructor out");
#endif
}

void CFilterObj::CleanUp()
{
    if (m_hFile != INVALID_HANDLE_VALUE)
	{
#ifdef _DIAGREL
	Log("Cleanup() - closing file");
#endif
		CloseHandle(m_hFile);
		m_hFile = INVALID_HANDLE_VALUE;
	}

	if (m_pStm != NULL)
	{
#ifdef _DIAGREL
	Log("Cleanup() - closing stream");
#endif
		m_pStm->Release();
		m_pStm = NULL;
	}

	if (m_wszBuffer != NULL)
	{
#ifdef _DIAGREL
	Log("Cleanup() - releasing buffer");
#endif
		SysFreeString(m_wszBuffer);
		m_wszBuffer = NULL;
	}
#ifdef _DIAGREL
	Log("Cleanup() - exit");
#endif
}

/////////////////////////////////////////////////////////////////////////////
// IFilter
//
HRESULT CFilterObj::Init(ULONG grfFlags, ULONG cAttributes, const FULLPROPSPEC *aAttributes, ULONG *pFlags)
{
#ifdef _DIAGREL
	Log("Init (v1.0.0.4) - grfFlags=0x%08x, cAttr=%d", grfFlags, cAttributes);
#endif

//	CleanUp();

/*
	m_nChunkId = 1;

	// TODO:  Perhaps - Init can specify the flavour of the text returnrd

	*pFlags = 0;

	// open the file
	m_hFile = CreateFileW(m_wszFilename, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_DELETE,
		0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

    if (m_hFile == INVALID_HANDLE_VALUE)
        return FILTER_E_ACCESS;
*/
    // Ignore flags for text canonicalization (text is unformatted)
    // Check for proper attributes request and recognize only "contents"

    if (cAttributes > 0)
    {
        ULONG ulNumAttr;

        if (aAttributes == NULL)
		{
            return E_INVALIDARG;
		}

        for (ulNumAttr = 0 ; ulNumAttr < cAttributes; ulNumAttr++)
        {
            if (guidStorage == aAttributes[ulNumAttr].guidPropSet &&
				PID_STG_CONTENTS == aAttributes[ulNumAttr].psProperty.propid)
			{
                break;
			}
        }

        if (ulNumAttr < cAttributes)
		{
            m_bContents = TRUE;
		}
        else
		{
            m_bContents = false;
		}
    }

    else if (grfFlags == 0 || (grfFlags & IFILTER_INIT_APPLY_INDEX_ATTRIBUTES))
	{
        m_bContents = true;
	}
    else
	{
        m_bContents = false;
	}

	if (*m_wszFilename != '\0')
	{
#ifdef _DIAGREL
		char* p = w2c(m_wszFilename);
		Log("  Open File - %s", p);
		free(p);
#endif
		// Open the file previously specified in call to IPersistFile::Load

		m_hFile = CreateFileW(
					  m_wszFilename,
					  GENERIC_READ,
					  FILE_SHARE_READ | FILE_SHARE_DELETE,
					  0,
					  OPEN_EXISTING,
					  FILE_ATTRIBUTE_NORMAL,
					  0
				  );

		if (m_hFile == INVALID_HANDLE_VALUE)
		{
#ifdef _DIAGREL
			Log("OpenFile failed.  Returning FILTER_E_ACCESS");
#endif
			return FILTER_E_ACCESS;
		}
	}
#ifdef _DIAGREL
	else if (m_pStm != NULL)
	{
		Log("  Open Stream");
	}
	else
	{
		Log("  Neither File nor Stream !!");
	}
#endif

    // Enumerate OLE properties, since any NTFS file can have them

//	*pFlags = IFILTER_FLAGS_OLE_PROPERTIES;
	*pFlags = 0;

    // Re-initialize

    m_nChunkId = 1;

	return S_OK;
}

HRESULT CFilterObj::GetChunk(STAT_CHUNK *pStat)
{
	try
	{
#ifdef _DIAGREL
	Log("GetChunk");
#endif


	// has the single chunk already been read ?
	if (m_nChunkId > 1 || !m_bContents)
	{
#ifdef _DIAGREL
		Log("GetChunk returned FILTER_E_END_OF_CHUNKS");
#endif
		return FILTER_E_END_OF_CHUNKS;
	}

	if (*m_wszFilename == 0 && m_pStm == NULL)
	{
#ifdef _DIAGREL
		Log("GetChunk returned FILTER_E_ACCESS (1)");
#endif
		return FILTER_E_ACCESS;
	}

/*
	typedef struct tagSTATSTG 
	{ 
	  LPWSTR          pwcsName; 
	  DWORD           type; 
	  ULARGE_INTEGER  cbSize; 
	  FILETIME        mtime; 
	  FILETIME        ctime; 
	  FILETIME        atime; 
	  DWORD           grfMode; 
	  DWORD           grfLocksSupported; 
	  CLSID           clsid; 
	  DWORD           grfStateBits; 
	  DWORD           reserved; 
	} STATSTG; 
*/
	HRESULT hr = S_OK;
	ULONG dwBufSize;

	if (m_pStm != NULL)
	{
#ifdef _DIAGREL
		Log("  GetChunk reads from stream");
#endif
		STATSTG stg;

		// Read all data from the stream.  Find its size first.
		if (FAILED(hr = m_pStm->Stat(&stg, STATFLAG_NONAME)))
		{
#ifdef _DIAGREL
		Log(" Stream read failed, hr = 0x%08x", hr);
#endif
			return hr;
		}

		// Allocate a buffer for HJsource
		dwBufSize = (ULONG) stg.cbSize.QuadPart;
	}
	else if (m_hFile != INVALID_HANDLE_VALUE)
	{
		dwBufSize = GetFileSize(m_hFile, NULL);
	}
	else
	{
#ifdef _DIAGREL
		Log("GetChunk returned FILTER_E_ACCESS (2)");
#endif
		return FILTER_E_ACCESS;
	}

#ifdef _DIAGREL
		Log("  Create buffer of %d bytes", dwBufSize);
#endif

	SAFEARRAY* psa = SafeArrayCreateVector(VT_UI1, 0, dwBufSize + 1);
	ULONG dwBytesRead = 0;

	if (m_pStm != NULL)
	{
#ifdef _DIAGREL
		Log("  Read Storage");
#endif
		hr = m_pStm->Read(psa->pvData, dwBufSize, &dwBytesRead);
		m_bPacked = (*(WORD*)psa->pvData != 0x1489);
#ifdef _DIAGREL
		Log("  ... read %d bytes from file", dwBytesRead);
		Log("  ... data is %spacked", m_bPacked ? "" : "un");
		Dump(psa->pvData, dwBytesRead, false);
#endif
	}

	else
	{
#ifdef _DIAGREL
		Log("  Read File");
#endif
		ReadFile(m_hFile, psa->pvData, dwBufSize, &dwBytesRead, NULL);
		m_bPacked = (*(WORD*)psa->pvData != 0x1489);
#ifdef _DIAGREL
		Log("  ... read %d bytes from file", dwBytesRead);
		Log("  ... data is %spacked", m_bPacked ? "" : "un");
		Dump(psa->pvData, dwBytesRead, false);
#endif
	}

	SafeArrayUnaccessData(psa);


	if (m_pStm != NULL)
	{
		// stream no longer required
		m_pStm->Release();
		m_pStm = NULL;
	}
	else
	{
		CloseHandle(m_hFile);
		m_hFile = INVALID_HANDLE_VALUE;
	}

	if (FAILED(hr))
	{
#ifdef _DIAGREL
		Log("GetChunk returned 0x%08x", hr);
#endif
		return hr;
	}

	// Convert the chunk
/*
	static WCHAR* wszText[] = {
		L"The quick brown fox",
		L"jumped over the lazy dog",
		L"Fe fi fi fum",
		L"She sells sea shells",
		L"by the sea shore",
		L"Mary had a little lamb",
		L"it's fleece was white as snow",
		L"the lamb was sure to go"
	};


	DWORD n = GetTickCount() & 0x07;
	m_wszBuffer = SysAllocString(wszText[n]);

*/
	// fire up hj2ascii
#ifdef _DIAGREL
		Log("  Calling HJ2Ascii");
#endif

	IHj2Ascii3Ptr pHJ3(__uuidof(Hj2Ascii3), NULL, CLSCTX_INPROC_SERVER);
	
	// No Hyphenation
	pHJ3->put_IgnoreNonSource(TRUE);
//	pHJ3->put_CharacterTranslationMode(1); // output printable chars only
	hr = pHJ3->put_CharacterTranslationMode(3); // output unicode chars
#ifdef _DIAGREL
	if (hr != 0)
	{
		Log("  ... hj-PutCharacterTranslationMode error 0x%08x", hr);
	}
#endif

	// Get source
	VARIANT varSrc;
	VariantInit(&varSrc);

	varSrc.vt = (VT_ARRAY | VT_UI1);
	varSrc.parray = psa;

	// Convert source to wide ascii
//	IHJ2AsciiPtr pHJ = pHJ3;
	hr = pHJ3->putSource(varSrc, m_bPacked);
#ifdef _DIAGREL
		Log("  ... hj-PutSource %d", hr);
#endif
	hr = pHJ3->FullConvert(&m_wszBuffer);

	m_nOffset = 0;
	m_nOutSize = wcslen(m_wszBuffer) + 1;

#ifdef _DIAGREL
		Log("  ... HJ2Ascii returned %d, output text = %d bytes", hr, m_nOutSize);
#endif
/*
	typedef struct tagSTAT_CHUNK
	{
		ULONG              idChunk;
		CHUNK_BREAKTYPE    breakType;
		CHUNKSTATE         flags;
		LCID               locale;
		FULLPROPSPEC       attribute;
		ULONG              idChunkSource;
		ULONG              cwcStartSource;
		ULONG              cwcLenSource;
	}  STAT_CHUNK;
*/

	pStat->idChunk = m_nChunkId;
//	pStat->breakType = CHUNK_NO_BREAK;
	pStat->breakType = CHUNK_EOW;
	pStat->flags = CHUNK_TEXT;
	pStat->locale = GetSystemDefaultLCID();
//	pStat->locale = 0x0c09;

	pStat->attribute.guidPropSet = guidStorage;
	pStat->attribute.psProperty.ulKind = PRSPEC_PROPID;
	pStat->attribute.psProperty.propid = PID_STG_CONTENTS;

	pStat->idChunkSource = m_nChunkId;
	pStat->cwcStartSource = 0;
	pStat->cwcLenSource = 0;

	m_nChunkId++;

#ifdef _DIAGREL
	Log("GetChunk returned S_OK");
#endif
	return S_OK;
	}
	catch (...)
	{
	}

#ifdef _DIAGREL
	Log("GetChunk threw");
#endif
	return E_FAIL;
}

HRESULT CFilterObj::GetText(ULONG *pcwcBuffer, WCHAR *awcBuffer)
{
#ifdef _DIAGREL
	Log("GetText");
#endif

	if (m_nOffset >= m_nOutSize || !m_bContents)
	{
#ifdef _DIAGREL
		Log("GetText returned FILTER_E_NO_MORE_TEXT");
#endif
		*pcwcBuffer = 0;
		return FILTER_E_NO_MORE_TEXT;
	}

	ULONG nRequired = min(*pcwcBuffer, m_nOutSize - m_nOffset);

	if (nRequired > 0)
	{
		wcsncpy(awcBuffer, m_wszBuffer + m_nOffset, nRequired);
		m_nOffset += nRequired;
	}

	*pcwcBuffer = nRequired;

#ifdef _DIAGREL
	Dump(awcBuffer, nRequired * 2, true);
//	char* p = w2c(awcBuffer);
//	Log("Text Size = %d\n"
//		"---------------------------------------\n"
//		"%s"
//		"---------------------------------------", nRequired, p);
//	free(p);
#endif

	if (m_nOffset >= m_nOutSize)
	{
		SysFreeString(m_wszBuffer);
		m_wszBuffer = NULL;

#ifdef _DIAGREL
		Log("GetText returned FILTER_S_LAST_TEXT");
#endif
		return FILTER_S_LAST_TEXT;
	}
	
#ifdef _DIAGREL
	Log("GetText returned S_OK");
#endif
	return S_OK;
}

HRESULT CFilterObj::GetValue(PROPVARIANT **ppPropValue)
{
#ifdef _DIAGREL
	Log("GetValue() - returned FILTER_E_NO_VALUES");
#endif
	return FILTER_E_NO_VALUES;
}

HRESULT CFilterObj::BindRegion(FILTERREGION origPos, REFIID riid, void **ppunk)
{
#ifdef _DIAGREL
	Log("BindRegion() - returned E_NOTIMPL");
#endif
	// Currently reserved for future use
	return E_NOTIMPL;
}


/////////////////////////////////////////////////////////////////////////////
// IPersist
HRESULT CFilterObj::GetClassID(CLSID *pClassID)
{
#ifdef _DIAGREL
	Log("GetClassID()");
#endif
    *pClassID = __uuidof(CFilterObj);
    return S_OK;
}

/////////////////////////////////////////////////////////////////////////////
// IPersistStream
HRESULT CFilterObj::IsDirty(void)
{
#ifdef _DIAGREL
	Log("IsDirty() - returned S_FALSE");
#endif
    // Stream is read-only and never changes
    return S_FALSE;
}

HRESULT CFilterObj::Load(IStream *pStm)
{
#ifdef _DIAGREL
	Log("Load Stream");
#endif

	// remember the stream
	m_pStm = pStm;
	m_pStm->AddRef();
	return S_OK;
}

HRESULT CFilterObj::Save(IStream *pStm, BOOL fClearDirty)
{
#ifdef _DIAGREL
	Log("Save(stream) - returned E_FAIL");
#endif
    // Stream is read-only and never changes
	return E_FAIL;
}

HRESULT CFilterObj::GetSizeMax(ULARGE_INTEGER *pcbSize)
{
#ifdef _DIAGREL
	Log("GetSizeMax() - returned E_FAIL");
#endif
    // Stream is read-only and never changes
	return E_FAIL;
}

/////////////////////////////////////////////////////////////////////////////
// IPersistFile
HRESULT CFilterObj::Load(LPCOLESTR pszFileName, DWORD dwMode)
{
#ifdef _DIAGREL
	char* p = w2c(pszFileName);
	Log("Load File - %s", p);
	free(p);
#endif

	// remember the filename
	wcscpy(m_wszFilename, pszFileName);

	return S_OK;
}

HRESULT CFilterObj::Save(LPCOLESTR pszFileName, BOOL fRemember)
{
#ifdef _DIAGREL
	Log("Save(file) - returned E_FAIL");
#endif
    // Stream is read-only and never changes
	return E_FAIL;
}

HRESULT CFilterObj::SaveCompleted(LPCOLESTR pszFileName)
{
#ifdef _DIAGREL
	Log("SaveCompleted(file) - returned E_FAIL");
#endif
    // Stream is read-only and never changes
	return E_FAIL;
}

HRESULT CFilterObj::GetCurFile(LPOLESTR __RPC_FAR *ppszFileName)
{
#ifdef _DIAGREL
	Log("GetCurFile() - returned E_FAIL");
#endif
    // Stream is read-only and never changes
	return E_FAIL;
}

