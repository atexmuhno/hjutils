
HJFilterps.dll: dlldata.obj HJFilter_p.obj HJFilter_i.obj
	link /dll /out:HJFilterps.dll /def:HJFilterps.def /entry:DllMain dlldata.obj HJFilter_p.obj HJFilter_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del HJFilterps.dll
	@del HJFilterps.lib
	@del HJFilterps.exp
	@del dlldata.obj
	@del HJFilter_p.obj
	@del HJFilter_i.obj
