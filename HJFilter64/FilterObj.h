// FilterObj.h : Declaration of the CFilter

#ifndef __FILTEROBJ_H_
#define __FILTEROBJ_H_

#include "resource.h"       // main symbols

class  __declspec(uuid("8E2F5685-C39B-41A4-90BA-89F7C9A3161D")) CFilterObj; 

/////////////////////////////////////////////////////////////////////////////
// CFilterObj
class ATL_NO_VTABLE CFilterObj : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<CFilterObj, &__uuidof(CFilterObj)>,
	public IFilter,
	public IPersistStream,
	public IPersistFile
{
public:
	CFilterObj();
	~CFilterObj();

	DECLARE_REGISTRY_RESOURCEID(IDR_FILTEROBJ)
	DECLARE_NOT_AGGREGATABLE(CFilterObj)

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	BEGIN_COM_MAP(CFilterObj)
		COM_INTERFACE_ENTRY(IFilter)
		COM_INTERFACE_ENTRY(IPersistStream)
		COM_INTERFACE_ENTRY(IPersistFile)
	END_COM_MAP()

public:
	// IFilter
	STDMETHOD(Init)(ULONG grfFlags, ULONG cAttributes, const FULLPROPSPEC *aAttributes, ULONG *pFlags);
	STDMETHOD(GetChunk)(STAT_CHUNK *pStat);
	STDMETHOD(GetText)(ULONG *pcwcBuffer, WCHAR *awcBuffer);
	STDMETHOD(GetValue)(PROPVARIANT **ppPropValue);
	STDMETHOD(BindRegion)(FILTERREGION origPos, REFIID riid, void **ppunk);

	// IPersist
	STDMETHOD(GetClassID)(CLSID *pClassID);

	// IPersistStream
	STDMETHOD(IsDirty)(void);
	STDMETHOD(Load)(IStream *pStm);
	STDMETHOD(Save)(IStream *pStm, BOOL fClearDirty);
	STDMETHOD(GetSizeMax)(ULARGE_INTEGER *pcbSize);

	// IPersistFile
//	STDMETHOD(IsDirty)(void);
	STDMETHOD(Load)(LPCOLESTR pszFileName, DWORD dwMode);
	STDMETHOD(Save)(LPCOLESTR pszFileName, BOOL fRemember);
	STDMETHOD(SaveCompleted)(LPCOLESTR pszFileName);
	STDMETHOD(GetCurFile)(LPOLESTR __RPC_FAR *ppszFileName);
        

private:
	void CleanUp();	

	IStream *m_pStm;
	long m_nChunkId;
    bool m_bContents;            // TRUE if contents requested

	BSTR  m_wszBuffer;
	ULONG m_nOffset;
	ULONG m_nOutSize;
	WCHAR m_wszFilename[256];
	HANDLE m_hFile;
	BOOL m_bPacked;
};
#endif
